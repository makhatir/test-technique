import React, { Component } from 'react';
import {Container, Row, Col,Alert, Card, CardBody, CardFooter, Button, Input, InputGroup, InputGroupAddon, InputGroupText,  NavLink} from 'reactstrap';
import { Route, Redirect } from 'react-router';
import 'whatwg-fetch';
import Header from './../../../components/Header/Header';
import Footer from './../../../components/Footer/Footer';
import {
  getFromStorage,
  setInStorage,
} from './../../../utils/storage';

class Register extends Component {
    constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      token: '',
      type: '',
      usersession: {} ,
      signUpError: '',
      signInError: '',
      signInEmail: '',
      signInPassword: '',
      signUpFirstName: '',
      signUpLastName: '',
      signUpEmail: '',
      signUpAgence: '',
    };

    this.onTextboxChangeSignInEmail = this.onTextboxChangeSignInEmail.bind(this);
    this.onTextboxChangeSignUpEmail = this.onTextboxChangeSignUpEmail.bind(this);
    this.onTextboxChangeSignUpAgence = this.onTextboxChangeSignUpAgence.bind(this);
    this.onTextboxChangeSignUpFirstName = this.onTextboxChangeSignUpFirstName.bind(this);
    this.onTextboxChangeSignUpLastName = this.onTextboxChangeSignUpLastName.bind(this);
    
    this.onSignIn = this.onSignIn.bind(this);
    this.onSignUp = this.onSignUp.bind(this);
    this.logout = this.logout.bind(this);
    this.loginIn = this.loginIn.bind(this);
  }

  componentDidMount() {
    const obj = getFromStorage('the_main_app');
      if(obj && obj.token){
            const {token} = obj;
           fetch('/api/account/signin')
           .then(res => res.json())
          
          
      }
 
    if (obj && obj.token) {
      const {token} = obj;
          
      //Verify token
      fetch('/api/account/verify?token=' + token)
    
      .then(res => res.json())
      .then(json => {
        if (json.success) {
          this.setState({
            token,
            isLoading: false,
          });
        } else {
          this.setState({
            isLoading: false,
          });
          console.log('eerrrr')
        }
      });
    } else {
      this.setState({
        isLoading: false,
      });
    }
     

  }
    
    

  onTextboxChangeSignInEmail(event) {
    this.setState({
      signInEmail: event.target.value,
    });
  }

 

  onTextboxChangeSignUpFirstName(event) {
    this.setState({
      signUpFirstName: event.target.value,
    });
  }
  
  onTextboxChangeSignUpLastName(event) {
    this.setState({
      signUpLastName: event.target.value,
    });
  }

  onTextboxChangeSignUpEmail(event) {
    this.setState({
      signUpEmail: event.target.value,
    });
  }

  onTextboxChangeSignUpAgence(event) {
    this.setState({
      signUpAgence: event.target.value,
    });
  }
 
  onSignUp() {
    // Grab state
    const {
      signUpFirstName,
      signUpLastName,
      signUpEmail,
      signUpAgence,
    } = this.state;

    this.setState({
      isLoading: true,
    });
    // Post request to backend
    fetch('/api/account/signup', { 
      method: 'POST',
       headers: {
         'Content-Type': 'application/json'
       },
      body: JSON.stringify({
        nom: signUpFirstName,
        prenom: signUpLastName,
        email: signUpEmail,
        agence: signUpAgence
      }),
      
    }).then(res => res.json())
      .then(json => {
        if(json.success){
          this.setState({
            signUpError: json.message,
            isLoading: false,
            signUpEmail: '',
            signUpAgence: '',
            signUpFirstName: '',
            signUpLastName:'',
          });
          
          console.log('sign up');
          fetch('/api/account/signin', { 
            method: 'POST',
             headers: {
               'Content-Type': 'application/json'
             },
            body: JSON.stringify({
              email: signUpEmail,
            
            }),
            
          }).then(res => res.json())
            .then(json => {
              if(json.success){
                setInStorage('the_main_app', { token: json.token});
                this.setState({
                  signInError: json.message,
                  isLoading: false,
                  signInEmail: '',
                  token: json.token,
                            });
                              location.reload();
              } else {
                this.setState({
                  signInError: json.message,
                  isLoading: false,
                });
              } 
            });
 
        } else {
          this.setState({
            signUpError: json.message,
            isLoading: false,
          });
        } 
      });
  }
 
  onSignIn() {
    // Grab state
    const {
      signInEmail,
      signInPassword,
    } = this.state;

    this.setState({
      isLoading: true,
    });
    // Post request to backend
    fetch('/api/account/signin', { 
      method: 'POST',
       headers: {
         'Content-Type': 'application/json'
       },
      body: JSON.stringify({
        email: signInEmail,
        
      }),
      
    }).then(res => res.json())
      .then(json => {
        if(json.success){
          setInStorage('the_main_app', { token: json.token});
          this.setState({
            signInError: json.message,
            isLoading: false,
            signInEmail: '',
            token: json.token,
          });
        } else {
          this.setState({
            signInError: json.message,
            isLoading: false,
          });
        } 
      });

  }

  logout() {
    this.setState({
      isLoading: true
    })
    const obj = getFromStorage('the_main_app');
    if (obj && obj.token) {
      const { token } = obj;
      //Verify token
      fetch('/api/account/logout?token=' + token)
      .then(res => res.json())
      .then(json => {
        if (json.success) {
          this.setState({
            token:'',
            isLoading: false,
          });
        } else {
          this.setState({
            isLoading: false,
          });
          console.log('eerrrr')
        }
      });
    } else {
      this.setState({
        isLoading: false,
      });
    }
  }
  render() {
    const {
      isLoading,
      token,
      signInError,
      signUpFirstName,
      signUpLastName,
      signUpEmail,
      signUpAgence,
      signUpError
      
    } = this.state;

    if (isLoading) {
      return(<div><p>Loading...</p></div>);
    }
    if (!token) {
      return (
        <div>
          <Header />
           <div className="app flex-row align-items-center app-sign">
        <Container>
          <Row className="justify-content-center">
            <Col md="6">
              <Card className="mx-4">
                <CardBody className="p-4">
                  <h1>Register</h1>
                  <p className="text-muted">Create your account</p>
                  
                <Alert color="primary">
                {
              (signUpError) ? (
                <h3>{signUpError}</h3>
              ) : (null)
            }
                </Alert>
         
                  <InputGroup className="mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="icon-user"></i>
                      </InputGroupText>
                    </InputGroupAddon>
                    
             <input 
              type="text"
              className="form-control"
              placeholder="Firstname" 
              value={signUpFirstName}
              onChange={this.onTextboxChangeSignUpFirstName}
              required
            />
             <input 
              type="text"
              className="form-control"
              placeholder="Lastname" 
              value={signUpLastName}
              onChange={this.onTextboxChangeSignUpLastName}
              required
            />
                  </InputGroup>
                  <InputGroup className="mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>@</InputGroupText>
                    </InputGroupAddon>
                        <input 
              type="email"
              className="form-control"
              placeholder="Email" 
              value={signUpEmail}
              onChange={this.onTextboxChangeSignUpEmail} 
              required autofocus
           />
                  </InputGroup>
            
                  <InputGroup className="mb-4">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="icon-lock"></i>
                      </InputGroupText>
                    </InputGroupAddon>
                      <Input type="select" value={signUpAgence} onChange= {this.onTextboxChangeSignUpAgence} required>
                        <option value="#">Please select Agence Sqli</option>
                         
                        <option value="Sqli oujda" >Sqli Oujda </option>
                        <option value="Sqli Rabat" >Sqli Rabat </option>
                                   
                      </Input>
                  </InputGroup>
                        <Button color="success" block onClick={this.onSignIn}>Create Account</Button>
                        
                </CardBody>
                <CardFooter className="p-4">
                  <Row>
                    <Col xs="12" sm="6">
                      <Button className="btn-facebook" block><span>facebook</span></Button>
                    </Col>
                    <Col xs="12" sm="6">
                      <Button className="btn-twitter" block><span>twitter</span></Button>
                    </Col>
                  </Row>
                </CardFooter>
              </Card>
            </Col>
          </Row>
          
        </Container>
         
                  
                  
      </div>
<Footer />
        </div>
      
      );
    }

    return (
      <div>
         <Redirect to="/login"/>
      </div>
    );
  }
}

export default Register;
