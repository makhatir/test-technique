import React, { Component } from 'react';
import { Route, Redirect } from 'react-router';
import {Container, Row, Alert,Col, CardGroup, Card, CardBody,CardFooter,NavLink, Button, Input, InputGroup, InputGroupAddon, InputGroupText} from 'reactstrap';
import {
  BrowserRouter as Router,
  Link,
  Switch
} from 'react-router-dom'

import 'whatwg-fetch';
import Header from './../../../components/Header/Header';
import Footer from './../../../components/Footer/Footer';
import {
  getFromStorage,
  setInStorage,
} from './../../../utils/storage';

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      token: '',
      temps: '',
      signUpError: '',
      signInError: '',
      signInEmail: '',
      signInPassword: '',
      signUpFirstName: '',
      signUpLastName: '',
      signUpEmail: '',
      signUpPassword: '',
    };

    this.onTextboxChangeSignInEmail = this.onTextboxChangeSignInEmail.bind(this);
    this.onTextboxChangeSignInPassword = this.onTextboxChangeSignInPassword.bind(this);
    this.onTextboxChangeSignUpEmail = this.onTextboxChangeSignUpEmail.bind(this);
    this.onTextboxChangeSignUpPassword = this.onTextboxChangeSignUpPassword.bind(this);
    this.onTextboxChangeSignUpFirstName = this.onTextboxChangeSignUpFirstName.bind(this);
    this.onTextboxChangeSignUpLastName = this.onTextboxChangeSignUpLastName.bind(this);
    
    this.onSignIn = this.onSignIn.bind(this);
    this.onSignUp = this.onSignUp.bind(this);
    this.logout = this.logout.bind(this);
    this.list_users = this.list_users.bind(this);
  }

  componentDidMount() {
    const obj = getFromStorage('the_main_app');
      if(obj && obj.token){
            const {token} = obj;
           fetch('/api/account/signin')
           .then(res => res.json())
          
          
      }
 
    if (obj && obj.token) {
      const {token} = obj;
          
      //Verify token
      fetch('/api/account/verify?token=' + token)
    
      .then(res => res.json())
      .then(json => {
        if (json.success) {
          this.setState({
            token,
            isLoading: false,
          });
        } else {
          this.setState({
            isLoading: false,
          });
          console.log('eerrrr')
        }
      });
    } else {
      this.setState({
        isLoading: false,
      });
    }
     

  }
    

  onTextboxChangeSignInEmail(event) {
    this.setState({
      signInEmail: event.target.value,
    });
  }

  onTextboxChangeSignInPassword(event) {
    this.setState({
      signInPassword: event.target.value,
    });
  }

  onTextboxChangeSignUpFirstName(event) {
    this.setState({
      signUpFirstName: event.target.value,
    });
  }
  
  onTextboxChangeSignUpLastName(event) {
    this.setState({
      signUpLastName: event.target.value,
    });
  }

  onTextboxChangeSignUpEmail(event) {
    this.setState({
      signUpEmail: event.target.value,
    });
  }

  onTextboxChangeSignUpPassword(event) {
    this.setState({
      signUpPassword: event.target.value,
    });
  }

  onSignUp() {
    // Grab state
    const {
      signUpFirstName,
      signUpLastName,
      signUpEmail,
      signUpPassword,
    } = this.state;

    this.setState({
      isLoading: true,
    });
    // Post request to backend
    fetch('/api/account/signups', { 
      method: 'POST',
       headers: {
         'Content-Type': 'application/json'
       },
      body: JSON.stringify({
        nom: signUpFirstName,
        prenom: signUpLastName,
        email: signUpEmail,
        password: signUpPassword
      }),
      
    }).then(res => res.json())
      .then(json => {
        if(json.success){
          this.setState({
            signUpError: json.message,
            isLoading: false,
            signUpEmail: '',
            signUpPassword: '',
            signUpFirstName: '',
            signUpLastName:'',
          });
        } else {
          this.setState({
            signUpError: json.message,
            isLoading: false,
          });
        } 
      });
  }
  list_users() {
    // Grab state
    const {
      signInEmail,
      signInPassword,
    } = this.state;

    this.setState({
      isLoading: true,
    });
      const obj = getFromStorage('the_main_app');
      const{email}=obj;
    // Get user to Front
    fetch('/api/account/signin', { 
      method: 'GET',
       headers: {
         'Content-Type': 'application/json'
       },
      body: JSON.stringify({
        email: signInEmail,
        password: signInPassword
      }),
      
    }).then(res => res.json())
      .then(json => {
        if(json.success){
          getFromStorage('the_main_app', { token: json.token});
          this.setState({
            signInError: json.message,
            isLoading: false,
            signInEmail: '',
            signInPassword: '',
            token: json.token,
            temps:json.temps
          });
        } else {
          this.setState({
            signInError: json.message,
            isLoading: false,
          });
        } 
      });

  }
  onSignIn() {
    // Grab state
    const {
      signInEmail,
      signInPassword,
    } = this.state;

    this.setState({
      isLoading: true,
    });
    // Post request to backend
    fetch('/api/account/signins', { 
      method: 'POST',
       headers: {
         'Content-Type': 'application/json'
       },
      body: JSON.stringify({
        email: signInEmail,
        password: signInPassword
      }),
      
    }).then(res => res.json())
      .then(json => {
        if(json.success){
          setInStorage('the_main_app', { token: json.token});
          this.setState({
            signInError: json.message,
            isLoading: false,
            signInEmail: '',
            signInPassword: '',
            token: json.token,
                      });
                        location.reload();
        } else {
          this.setState({
            signInError: json.message,
            isLoading: false,
          });
        } 
      });

  }

  logout() {
     location.reload();
    this.setState({
      isLoading: true
    })
    const obj = getFromStorage('the_main_app');
    if (obj && obj.token) {
      const { token } = obj;
      //Verify token
      fetch('/api/account/logout?token=' + token)
      .then(res => res.json())
      .then(json => {
        if (json.success) {
          this.setState({
            token:'',
            isLoading: false,
              
          });
        } else {
          this.setState({
            isLoading: false,
          });
          console.log('eerrrr')
        }
      });
    } else {
      this.setState({
        isLoading: false,
      });
    }
  }
  keydowns(){
    console.log("keydown");
    {this.onSignIn}
  }
  render() {
    const {
      isLoading,
      token,
      signInError,
      signInEmail,
      signInPassword,
      signUpFirstName,
      signUpLastName,
      signUpEmail,
      signUpPassword,
      signUpError,
        
      
    } = this.state;

    if (isLoading) {
      return(<div><p>Loading...</p></div>);
    }
    if (!token) {
      return (
        <div className="app-sign">
<Header/>
<div className="app flex-row align-items-center">
        <Container>
        <Row className="justify-content-center">
        <Col md="6">
              <CardGroup>
                <Card className="p-4">
                  <CardBody onKeyDown={this.keydowns}>
                    <h1>Login</h1>
                    <p className="text-muted">Sign In to your account</p>
              {
              (signInError) ? (
                <p>{signInError}</p>
              ) : (null)
            }
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-user"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                        <input 
                  type="email"
                  className="form-control"
                  placeholder="Email" 
                  value={signInEmail}
                  onChange={this.onTextboxChangeSignInEmail}
                  required autofocus
                  onKeyPress={(event) => {
                    if (event.key === "Enter") {
                      this.onSignIn()
                    }
                  }}
              />
                    </InputGroup>
                    <InputGroup className="mb-4">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-lock"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                           <input 
                  type="password"
                  className="form-control"
                  placeholder="Password" 
                  value={signInPassword} 
                  onChange={this.onTextboxChangeSignInPassword}
                  required
                  onKeyPress={(event) => {
                    if (event.key === "Enter") {
                      this.onSignIn()
                    }
                  }}
              />
                    </InputGroup>
                    <Row>
                      <Col xs="6">
                        <Button color="primary" onClick={this.onSignIn} className="px-4">Login</Button>
                      </Col>
                      <Col xs="6" className="text-right">
                        <Button color="link" className="px-0">Forgot password?</Button>

                      </Col>
                    </Row>
                  </CardBody>
                  <CardFooter>

                  </CardFooter>
                </Card>
               
              </CardGroup>
            </Col>
            <Col md="6">
              <Card className="mx-4">
                <CardBody className="p-4">
                  <h1>Register</h1>
                  <p className="text-muted">Create your account</p>
                  
                <Alert color="primary">
                {
              (signUpError) ? (
                <h3>{signUpError}</h3>
              ) : (null)
            }
                </Alert>
         
                  <InputGroup className="mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="icon-user"></i>
                      </InputGroupText>
                    </InputGroupAddon>
                    
             <input 
              type="text"
              className="form-control"
              placeholder="Firstname" 
              value={signUpFirstName}
              onChange={this.onTextboxChangeSignUpFirstName}
              required
            />
             <input 
              type="text"
              className="form-control"
              placeholder="Lastname" 
              value={signUpLastName}
              onChange={this.onTextboxChangeSignUpLastName}
              required
            />
                  </InputGroup>
                  <InputGroup className="mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>@</InputGroupText>
                    </InputGroupAddon>
                        <input 
              type="email"
              className="form-control"
              placeholder="Email" 
              value={signUpEmail}
              onChange={this.onTextboxChangeSignUpEmail} 
              required autofocus
           />
                  </InputGroup>
                  <InputGroup className="mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="icon-lock"></i>
                      </InputGroupText>
                    </InputGroupAddon>
                       <input 
              type="password"
              className="form-control" 
              placeholder="Password" 
              value={signUpPassword}
              onChange={this.onTextboxChangeSignUpPassword}
              required
           />
                  </InputGroup>
                  <InputGroup className="mb-4">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="icon-lock"></i>
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input type="password" placeholder="Repeat password"/>
                  </InputGroup>
                        <Button color="success" block onClick={this.onSignUp}>Create Account</Button>
                </CardBody>
               
                 
              </Card>
            </Col>

          </Row>
          </Container>

              
      </div>
      <Footer/>
        </div>
      );
    }

    return (
    
      <div>
       
       
       <Redirect push to="/dashbord" onClick={location.reload()}/>
        
       
      </div>
    );
  }
}

export default Login;
