import React from 'react';
import ReactDOM from 'react-dom';
import { render } from 'react-dom';

import {
  BrowserRouter as Router,
  Route,
 HashRouter,
  Link,
  Switch
} from 'react-router-dom';

import 'flag-icon-css/css/flag-icon.min.css';
import 'font-awesome/css/font-awesome.min.css';
import 'simple-line-icons/css/simple-line-icons.css';
import './styles/core/_dropdown-menu-right.scss'
import './styles/styles.scss';

import Full from './containers/Full/'
import Login from './views/Pages/Login/';
import Register from './views/Pages/Register/';
import Page404 from './views/Pages/Page404/'
import Page500 from './views/Pages/Page500/'

 // Import Main styles for this application
 import registerServiceWorker from './registerServiceWorker';

 
 

ReactDOM.render((
  <HashRouter>
      <Switch>
        
        <Route exact path="/register" component={Register}/>
       
        <Route exact path="/404" name="Page 404" component={Page404}/>
          <Route path="/wp-login" name="Login" component={Login}/>
        <Route exact path="/500" name="Page 500" component={Page500}/>
        <Route path="/" name="Home" component={Full}/>
     
      </Switch>
   </HashRouter>
   
), document.getElementById('app'));
   