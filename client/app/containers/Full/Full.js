import React, {Component} from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';
import Auth from './../../components/Auth/Auth';
import {Container} from 'reactstrap';

import Homepage from '../../components/Home/Home';
//--------- categorie

// pack Qcm
import ListQcms from '../../components/StudentMode/ListQcms';
import Concours from '../../components/StudentMode/Concours';
import ShowQcm from '../../components/Qcms/ShowQcm';
import AddQcm from '../../components/Qcms/AddQcm';
import EditQcm from '../../components/Qcms/EditQcm';
import DeleteQcm from '../../components/Qcms/DeleteQcm';
import Qcms from '../../views/AddQcm/Qcms/';
//-----------------------

// collab


import ShowCollab from '../../components/Collab/ShowCollab';
import AddCollab from '../../components/Collab/CreateCollab';
import EditCollab from '../../components/Collab/EditCollab';
import DeleteCollab from '../../components/Collab/DeleteCollab';
import ListCollab from '../../components/Collab/ListCollab';
// File
import ListFile from '../../components/File/ListFile';
import NewFile from '../../components/File/NewFile';


//-------- nav
import Aside from './../../components/Aside/';
import Header from './../../components/Header/';
import Header_Student from './../../components/Header/Header_Student';
import Footer from './../../components/Footer/';
import Breadcrumb from './../../components/Breadcrumb/';
import Sidebar from '../../components/Sidebar/';
// pack User
import CreateUser from '../../components/ListUser/Create';
import EditUser from '../../components/ListUser/Edit';
import ListUser from '../../components/ListUser/List';
import ShowUser from '../../components/ListUser/Show';
import DeleteUser from '../../components/ListUser/Delete';
// pack Profil
import Profile from '../../components/Profile/Profile';
import ShowP from '../../components/Profile/ShowP';
import EditP from '../../components/Profile/EditP';
import ShowPuser from '../../components/Profile/ShowPuser';

//------ Student Mode 
import ShowCtgQcm from '../../components/StudentMode/ShowCtgQcm';
import AppQcm from '../../components/StudentMode/AppQcm';
 
import {
  getFromStorage,
  setInStorage,
} from '../../utils/storage';

import axios from 'axios';
// Base
 
import Dashboard from '../../views/Dashboard/';
import Charts from '../../views/Charts/';
import Widgets from '../../views/Widgets/';
import Cards from '../../views/Base/Cards/';
import Forms from '../../views/Base/Forms/';
import Switches from '../../views/Base/Switches/';
import Tables from '../../views/Base/Tables/';
import Tabs from '../../views/Base/Tabs/';
import Breadcrumbs from '../../views/Base/Breadcrumbs/';
import Carousels from '../../views/Base/Carousels/';
import Collapses from '../../views/Base/Collapses/';
import Dropdowns from '../../views/Base/Dropdowns/';
import Jumbotrons from '../../views/Base/Jumbotrons/';
import ListGroups from '../../views/Base/ListGroups/';
import Navbars from '../../views/Base/Navbars/';
import Navs from '../../views/Base/Navs/';
import Paginations from '../../views/Base/Paginations/';
import Popovers from '../../views/Base/Popovers/';
import ProgressBar from '../../views/Base/ProgressBar/';
import Tooltips from '../../views/Base/Tooltips/';

// Buttons
import Buttons from '../../views/Buttons/Buttons/';
import ButtonDropdowns from '../../views/Buttons/ButtonDropdowns/';
import ButtonGroups from '../../views/Buttons/ButtonGroups/';
import SocialButtons from '../../views/Buttons/SocialButtons/';

// Icons
import Flags from '../../views/Icons/Flags/';
import FontAwesome from '../../views/Icons/FontAwesome/';
import SimpleLineIcons from '../../views/Icons/SimpleLineIcons/';

// Notifications
import Alerts from '../../views/Notifications/Alerts/';
import Badges from '../../views/Notifications/Badges/';
import Modals from '../../views/Notifications/Modals/';
import Login from '../../views/Pages/Login/';

import Layout from './../../layout'


class Full extends Auth {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      token: '',
      type: '',
      usersession: {} ,
    };
  }
 
  componentDidMount() {
     
    const obj = getFromStorage('the_main_app');
      if(obj && obj.token){
            const {token} = obj;
           fetch('/api/account/signin')
           .then(res => res.json()) 
      }
  
    if (obj && obj.token) {
      const {token} = obj;
          
      //Verify token
      fetch('/api/account/verify?token=' + token)
    
      .then(res => res.json())
      .then(json => {
        if (json.success) {
          this.setState({
            token,
            isLoading: false,
          });
          axios.get('/api/account/usersession/'+ token)
          .then(res => {
        
            this.setState({ usersession: res.data });
            
     
          });
        } else {
          this.setState({
            isLoading: false,
          });
     
        }
      });
    } else {
      this.setState({
        isLoading: false,
      });
    }
  
  
  }
  render() {
    const {
      isLoading,
      token,
      type,
      usersession: {} 
     
    } = this.state;

    if (isLoading) {
      return(<div><p>Loading...</p></div>);
    }
    if (!token) {
    return (
      <div className="app">
      <div className="app-login">
       <Header />
        <div className="app-home">
         
           
              <Switch>

               <Route path="/" name="Homepage" component={Homepage}/>
               
    
                 
              </Switch>
             
            
        
          
        </div>
         
          </div>
        <Footer />
      </div>
       
       
       
    );
  }
  else if(this.state.usersession.type=='admin' )
  {
       return (
      <div className="app">
        <Header/>
        <div className="app-body">
          <Sidebar {...this.props}/>
          <main className="main">
            <Breadcrumb/>
            <Container fluid>
              <Switch>
               
                  //route Profile

                  <Route path="/home" name="Profile" component={Profile}/>
              <Route path="/EditProfile/:userId" name="EditP" component={EditP}/>
              <Route path="/showP/:userId" name="ShowP" component={ShowP}/>
              //---------------------------------------------   
                 // route   Users
                <Route path="/users/createUser" name="CreateUser" component={CreateUser}/>
                <Route path="/users/editUser/:userId" name="EditUser" component={EditUser}/>
                <Route path="/users/listUser" name="ListUser" component={ListUser}/>
                <Route path="/users/showUser/:userId" name="ShowUser" component={ShowUser}/>
                <Route path="/users/deleteUser/:userId" name="DeleteUser" component={DeleteUser}/>
                //--------------------------------------------- 
              // route qcm
            <Route path="/listqcms" name="ListQcm" component={ListQcms}/>
            <Route path="/editQcm/:_id" name="EditQcm" component={EditQcm}/>
            <Route path="/deleteQcm/:_id" name="DeleteQcm" component={DeleteQcm}/>
            <Route path="/showQcm/:_id" name="ShowQcm" component={ShowQcm}/>
            <Route path="/addQcm/qcms" name="Qcms" component={Qcms}/>
             <Route path="/appQcm/:_id" name="AppQcm" component={AppQcm}/>
              
              //--------------------------------------------- 
              // route collab
            <Route path="/listcollab" name="ListCollab" component={ListCollab}/>
            <Route path="/editcollab/:_id" name="EditCollab" component={EditCollab}/>
            <Route path="/deletecollab/:_id" name="DeleteCollab" component={DeleteCollab}/>
            <Route path="/showcollab/:_id" name="ShowCollab" component={ShowCollab}/>
             <Route path="/addcollab" name="AddCollab" component={AddCollab}/>
              
             //----------------------------
             // route upload
             <Route path="/listFile" name="ListFile" component={ListFile}/>
             <Route path="/upload" name="upload" component={NewFile}/>
               //-----------------------------
                <Route path="/dashboard" name="Dashboard" component={Dashboard}/>
                <Route path="/base/cards" name="Cards" component={Cards}/>
                <Route path="/base/forms" name="Forms" component={Forms}/>
                <Route path="/base/switches" name="Swithces" component={Switches}/>
                <Route path="/base/tables" name="Tables" component={Tables}/>
                <Route path="/base/tabs" name="Tabs" component={Tabs}/>
                <Route path="/base/breadcrumbs" name="Breadcrumbs" component={Breadcrumbs}/>
                <Route path="/base/carousels" name="Carousels" component={Carousels}/>
                <Route path="/base/collapses" name="Collapses" component={Collapses}/>
                <Route path="/base/dropdowns" name="Dropdowns" component={Dropdowns}/>
                <Route path="/base/jumbotrons" name="Jumbotrons" component={Jumbotrons}/>
                <Route path="/base/list-groups" name="ListGroups" component={ListGroups}/>
                <Route path="/base/navbars" name="Navbars" component={Navbars}/>
                <Route path="/base/navs" name="Navs" component={Navs}/>
                <Route path="/base/paginations" name="Paginations" component={Paginations}/>
                <Route path="/base/popovers" name="Popovers" component={Popovers}/>
                <Route path="/base/progress-bar" name="Progress Bar" component={ProgressBar}/>
                <Route path="/base/tooltips" name="Tooltips" component={Tooltips}/>
                <Route path="/buttons/buttons" name="Buttons" component={Buttons}/>
                <Route path="/buttons/button-dropdowns" name="ButtonDropdowns" component={ButtonDropdowns}/>
                <Route path="/buttons/button-groups" name="ButtonGroups" component={ButtonGroups}/>
                <Route path="/buttons/social-buttons" name="Social Buttons" component={SocialButtons}/>
                <Route path="/icons/flags" name="Flags" component={Flags}/>
                <Route path="/icons/font-awesome" name="Font Awesome" component={FontAwesome}/>
                <Route path="/icons/simple-line-icons" name="Simple Line Icons" component={SimpleLineIcons}/>
                <Route path="/notifications/alerts" name="Alerts" component={Alerts}/>
                <Route path="/notifications/badges" name="Badges" component={Badges}/>
                <Route path="/notifications/modals" name="Modals" component={Modals}/>
                <Route path="/widgets" name="Widgets" component={Widgets}/>
                <Route path="/charts" name="Charts" component={Charts}/>
                <Redirect from ="/" to="/home"/>


              </Switch>
            </Container>
          </main>
         
         <Aside/>
        </div>
          <Footer/>
      </div>
    );   
  }
  else (this.state.usersession.type=='user') 
   { 
  return (
   <div className="app">

 
 <Header_Student/>
 
      <main>
      <Breadcrumb/>
      <Switch>
      <Route path="/home" name="Profile" component={Profile}/>
      </Switch>
            <Container fluid>
     
     <Switch>
         <Route path="/ShowCtgQcm/:_id" name="ShowCtgQcm" component={ShowCtgQcm}/>     
     <Route path="/EditProfile/:userId" name="EditP" component={EditP}/>
      <Route path="/showP/:_id" name="ShowPuser" component={ShowPuser}/>
 
     <Route path="/appQcm/:_id" name="AppQcm" component={AppQcm}/>
         <Route path="/showQcm/:_id" name="ShowQcm" component={ShowQcm}/>
         <Route path="/upload" name="Upload" component={ Layout}/>

        <Redirect from ="/" to="/home"/>
                 </Switch>
        
                 
                 </Container>
                 </main>
             
           </div>
          
); 
}  
}
}


export default Full;
