import React, { Component } from 'react';
import { Route, Redirect } from 'react-router';
import Layout from './../../layout'
import $ from 'jquery';
import DataTable from 'datatables.net';

import {
    Row,
    Col,
    Button,
    ButtonDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Card,
    CardHeader,
    CardFooter,
    CardBody,
    Collapse,
    Form,
    FormGroup,
    FormText,
    Label,
    Input,
    Table,
    InputGroup,
    InputGroupAddon,
    InputGroupText
  } from 'reactstrap';
import {
  BrowserRouter as Router,
  Link,
  Switch
} from 'react-router-dom'

import 'whatwg-fetch';
import axios from 'axios';

import {
  getFromStorage,
  setInStorage,
} from '../../utils/storage';

class EditQcm extends Component {

  constructor(props) {
    super(props);

    this.state = {
      qcms: {},
      files:[],
      posts:[],
      questions:[],
      reponse:[],
      Id:[],
      Question:'',
      Reponse:'',
      data:[],
    
    }; 
     this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onTextboxChangeQuestion = this.onTextboxChangeQuestion.bind(this);
    this.onTextboxChangeReponse = this.onTextboxChangeReponse.bind(this);
    this.AddQuestion = this.AddQuestion.bind(this);
    this.AddReponse = this.AddReponse.bind(this);
    this.DeleteQuest = this.DeleteQuest.bind(this);
    
    
  }

  onTextboxChangeQuestion(event) {
    this.setState({
      Question: event.target.value,
    });
  }
  onTextboxChangeReponse(event) {
    this.setState({
      Reponse: event.target.value,
    });
  }
      onChange(event) {
          const state = this.state.qcms;
    state[event.target.name] = event.target.value;
    this.setState({value: event.target.value});
  }

  onSubmit(event) {
    alert(' Qcm : ' + this.state.qcms.titre + ' is update');
    event.preventDefault();
          const { titre, description, duree,score,src, level, categorie,questions} = this.state.qcms;

    axios.put('/api/account/qcms/'+this.props.match.params._id, { titre, description, duree,score,src, level, categorie,questions})
      .then((result) => {
        this.props.history.push("/editQcm/"+this.props.match.params._id)
      });
  }
  


  AddQuestion(event) {
    alert(' question : ' + this.state.Question + ' is Added');
    event.preventDefault();
         
          const { 
            Question,
            Reponse,
                } = this.state;
          
          var questions=this.state.qcms.questions;
        var newId=this.state.Id;
          var arr={id:newId+1,question:Question,correct:'c',reponse:{id:'c',rep:Reponse}};

          questions.push(arr);

    axios.put('/api/account/qcms/'+this.props.match.params._id, {questions,arr})
      .then((result) => {
        this.props.history.push("/editqcm/"+this.props.match.params._id)
      
        
      
      });
  }

  AddReponse(event) {
    alert(' reponse : ' + this.state.Reponse + ' is Added');
    event.preventDefault();
         
          const { 
            Reponse,
                } = this.state;
          
          var reponse=this.state.REPO.reponse;
        
          var arr={rep:Reponse};

          reponse.push(arr);

    axios.put('/api/account/qcms/'+this.props.match.params._id, {reponse,arr})
      .then((result) => {
        console.log(reponse,arr)
        this.props.history.push("/editqcm/"+this.props.match.params._id)
      
        
      
      });
  }
  DeleteQuest(event) {
   
    event.preventDefault();
         
          const { 
            Question,
                } = this.state;
          
          var questions=this.state.qcms.questions;
          
          var arr={question:Question};
          
          
          questions.splice(0,1);
          

    axios.put('/api/account/qcms/'+this.props.match.params._id, {questions,arr})
      .then((result) => {
        this.props.history.push("/editqcm/"+this.props.match.params._id)
      
        
      
      });
      
          
      
   
  }
      componentDidMount() {
    axios.get('/api/account/qcms/'+this.props.match.params._id)
         
      .then(res => {
         console.log('eror');
        this.setState({ qcms: res.data });
        console.log(this.state.qcms['questions']);
        this.setState({
          data: this.state.qcms['questions']
        })
        var numb =this.state.data.length;
        this.setState({
          Id: this.state.qcms['questions'][numb-1].id
        })
        this.setState({
          REPO: this.state.qcms.questions[0]
        })
        
        console.log('Id',this.state.Id);
        console.log('reponse',this.state.REPO[0].reponse);
        
        
        console.log('data',this.state.data);
        var cat =this.state.data;
        cat.map((info,i) => 
        info.reponse.map(inf => 
         
          console.log('reep',inf.rep)
        )
   
        )
    
      });

      axios.get('/api/account/file')
      .then(res => {
        this.setState({ files: res.data });
     
      }); 
      axios.get('/api/account/post')
      .then(res => {
        this.setState({ posts: res.data });
 
      }); 
       
  }
 

 
  render() {
    
      $(document).ready( function () {
        setTimeout(function(){
          $('#ListFiles').DataTable();
        }, 400);
      });
     const { titre, description, duree,score,src, level, categorie,questions, Question,
      Reponse, 
    } = this.state;
    var cat =this.state.data;
    return (
    
     
       <div>
             <h2 className="text-center"> Edit Test  </h2>
        <Row>  <Col xs="12" md="12">
             <CardBody>
        <Form onSubmit={this.onSubmit}>
        <FormGroup row>
        <Col md="3">
                <Label >Titre:</Label>
                </Col>
                <Col xs="12" md="9">
                <Input type="text"   name="titre" value={this.state.qcms.titre} onChange={this.onChange} placeholder="titre" />
                </Col>
                </FormGroup>
                <FormGroup row>
                <Col md="3">
                <Label for="description">Description:</Label>
                </Col>
                <Col xs="12" md="9">
                <Input type="text"  name="description" value={this.state.qcms.description} onChange={this.onChange} placeholder="prenom" />
                </Col>
                </FormGroup>
                <FormGroup row>
                <Col md="3">
                <Label for="duree">Duree:</Label>
                </Col>
                <Col xs="12" md="9">
                <Input type="text"   name="duree" value={this.state.qcms.duree} onChange={this.onChange} placeholder="duree" />
                </Col>
                </FormGroup>
                <FormGroup row>
                <Col md="3">
                <Label for="Score">Score:</Label>
                </Col>
                <Col xs="12" md="9">
                <Input type="text"   name="score" value={this.state.qcms.score} onChange={this.onChange} placeholder="score" />
                </Col>
                </FormGroup>
                <FormGroup row>
                <Col md="3">
                <Label for="Src">Src:</Label>
                </Col>
                <Col xs="12" md="9">
                <Input type="text"   name="src" value={this.state.qcms.src} onChange={this.onChange} placeholder="Src" />
                </Col>
                </FormGroup>
                 
                 
              <CardFooter className="text-center">
               
              <Button type="submit" className="btn btn-default">Submit</Button>
              </CardFooter>
            </Form>
            </CardBody>
            </Col>
            <Col xs="12" md="12">
           <Card>
              <CardHeader>
                <strong>Upload File </strong>
                
              </CardHeader>
              <CardBody>
                <Layout />
              </CardBody>
              
              <CardFooter>
                
                
               
              </CardFooter>
              </Card>
          </Col>
          
<Col xs="12" md="12">
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> list File
              </CardHeader>
              <CardBody>
                <Table id="ListFiles" responsive bordered>
                  <thead>
                  <tr>

                  <th>From</th>
                  <th>Tests</th>
                  <th>Message</th>
                  
                  <th>Download</th>
                  <th>Operation</th>
                  </tr>
                  </thead>
                  <tbody>
                  {this.state.posts.map(post =>
              <tr key={post._id}>
                <th  scope="row"><Link to={`/users/showUser/${post._id}`} >{post.from}</Link></th> 


                <th>{post.to}</th>
                <th>{post.message}</th>
                <th> 
                <a  href={`http://localhost:3000/api/download/${post.files[0]}`}  className="btn btn-sm btn-primary"><i className="fa fa-pencil"></i> Download</a>
                </th>
                  <th>
                    <Link  to={`/users/showUser/${post._id}`}  className="btn btn-sm btn-primary"><i className="fa fa-pencil"></i> Show</Link>
                    <Link to={`/users/editUser/${post._id}`}  className="btn btn-sm btn-success"><i className="fa fa-pencil"></i> Edit</Link>
                    <Link to={`/users/deleteUser/${post._id}`}  className="btn btn-sm btn-danger"><i className="fa fa-pencil"></i> Delete</Link>
                    
                      </th>
              </tr>
            )}
                 
  
                  </tbody>
                </Table>
                 
              </CardBody>
            </Card>
          </Col>
          </Row>
      </div>

       
    );
  }
}

export default EditQcm;
