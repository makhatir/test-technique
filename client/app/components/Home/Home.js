import React, { Component } from 'react';
import {
  Nav,
  NavbarBrand,
  NavbarToggler,
  NavItem,
  NavLink,
 

} from 'reactstrap';
import Auth from '../Auth/Auth';
import Footer from'../Footer/Footer';
import { Route, Redirect } from 'react-router';
import { BrowserRouter as Router} from 'react-router-dom';
import {Container, Row, Col,Alert, Card, CardBody, CardFooter, CardGroup, Button, Input, InputGroup, InputGroupAddon, InputGroupText, Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap';

import 'whatwg-fetch';

import {
  getFromStorage,
  setInStorage,
} from '../../utils/storage';
 

class Home extends Component {
 constructor(props) {
    super(props);
    this.state = {
      modal: false,
       isLoading: true,
      token: '',
      type: '',
      usersession: {} ,
      signUpError: '',
      signInError: '',
      signInEmail: '',
      signInPassword: '',
      signUpFirstName: '',
      signUpLastName: '',
      signUpEmail: '',
      signUpAgence: '',
    };
    this.onTextboxChangeSignInEmail = this.onTextboxChangeSignInEmail.bind(this);
    this.onTextboxChangeSignUpEmail = this.onTextboxChangeSignUpEmail.bind(this);
    this.onTextboxChangeSignUpAgence = this.onTextboxChangeSignUpAgence.bind(this);
    this.onTextboxChangeSignUpFirstName = this.onTextboxChangeSignUpFirstName.bind(this);
    this.onTextboxChangeSignUpLastName = this.onTextboxChangeSignUpLastName.bind(this);
    
    this.onSignIn = this.onSignIn.bind(this);
    this.onSignUp = this.onSignUp.bind(this);
    this.toggle = this.toggle.bind(this);
  }
   toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }
 componentDidMount() {
    const obj = getFromStorage('the_main_app');
      if(obj && obj.token){
            const {token} = obj;
           fetch('/api/account/signin')
           .then(res => res.json())
          
          
      }
 
    if (obj && obj.token) {
      const {token} = obj;
          
      //Verify token
      fetch('/api/account/verify?token=' + token)
    
      .then(res => res.json())
      .then(json => {
        if (json.success) {
          this.setState({
            token,
            isLoading: false,
          });
        } else {
          this.setState({
            isLoading: false,
          });
          console.log('eerrrr')
        }
      });
    } else {
      this.setState({
        isLoading: false,
      });
    }
     

  }
    
    

  onTextboxChangeSignInEmail(event) {
    this.setState({
      signInEmail: event.target.value,
    });
  }

 

  onTextboxChangeSignUpFirstName(event) {
    this.setState({
      signUpFirstName: event.target.value,
    });
  }
  
  onTextboxChangeSignUpLastName(event) {
    this.setState({
      signUpLastName: event.target.value,
    });
  }

  onTextboxChangeSignUpEmail(event) {
    this.setState({
      signUpEmail: event.target.value,
    });
  }

  onTextboxChangeSignUpAgence(event) {
    this.setState({
      signUpAgence: event.target.value,
    });
  }
   onSignIn() {
    // Grab state
    const {
      signUpEmail,
     
    } = this.state;

    this.setState({
      isLoading: true,
    });
    fetch('/api/account/signin', { 
      method: 'POST',
       headers: {
         'Content-Type': 'application/json'
       },
      body: JSON.stringify({
        email: signUpEmail,
      
      }),
      
    }).then(res => res.json())
      .then(json => {
        if(json.success){
          setInStorage('the_main_app', { token: json.token});
          this.setState({
            signInError: json.message,
            isLoading: false,
            signUpEmail: '',
            token: json.token,
                      });
                        location.reload();
        } else {
          this.setState({
            signInError: json.message,
            isLoading: false,
          });
        } 
      });
    

  }
  onSignUp() {
    // Grab state
    const {
      signUpFirstName,
      signUpLastName,
      signUpEmail,
      signUpAgence,
    } = this.state;

    this.setState({
      isLoading: true,
    });
    // Post request to backend
    fetch('/api/account/signup', { 
      method: 'POST',
       headers: {
         'Content-Type': 'application/json'
       },
      body: JSON.stringify({
        nom: signUpFirstName,
        prenom: signUpLastName,
        email: signUpEmail,
        agence: signUpAgence
      }),
      
    }).then(res => res.json())
      .then(json => {
        if(json.success){
          this.setState({
            signUpError: json.message,
            isLoading: false,
            signUpEmail: '',
            signUpAgence: '',
            signUpFirstName: '',
            signUpLastName:'',
          });
          
          console.log('sign up');
          fetch('/api/account/signin', { 
            method: 'POST',
             headers: {
               'Content-Type': 'application/json'
             },
            body: JSON.stringify({
              email: signUpEmail,
            
            }),
            
          }).then(res => res.json())
            .then(json => {
              if(json.success){
                setInStorage('the_main_app', { token: json.token});
                this.setState({
                  signInError: json.message,
                  isLoading: false,
                  signInEmail: '',
                  token: json.token,
                            });
                              location.reload();
              } else {
                this.setState({
                  signInError: json.message,
                  isLoading: false,
                });
              } 
            });
 
        } else {
          this.setState({
            signUpError: json.message,
            isLoading: false,
          });
        } 
      });
  }
  render() {
    const {
      isLoading,
        token,
      signInError,
      signUpFirstName,
      signUpLastName,
      signUpEmail,
      signUpAgence,
      signUpError
       
    } = this.state;

    if (isLoading) {
      return(<div><p>Loading...</p></div>);
    }
    if (!token) {
      return (
        <div>
                
     
   
<div className="header-section ">
      <div className="container">
       
 
            <div className="text-center text-white p-5">
            <h1 className="display-3 p-3">TEST- SQLI</h1>
            <p className="lead text-center p-1">Join over programmers and improve your skills</p>
                 
      
      
       
      <div className="logo-sqli  p-3"></div>
          
        
        <p className="p-3">
                    <button className="btn btn-secondary signup-button fw p-1" onClick={this.toggle}> 
                  <a  className="mt-0 btn btn-primary active">   Sign Up &amp; Start Coding  !</a>

         
                    
                    </button>
                  </p>
                  
                  <p class=" text-center p-3">
               By signing up you agree to our Terms of Service and Privacy Policy
             </p>
                  <div className="hire-talent-wrapper p-3">
            <p className="hiring-label">
              Hiring Talent? <a className="demo-link" target="_blank" href="/#/contact" data-analytics="HiringTalent">Contact Us</a>
            </p>
          </div>
    </div>
    </div>
</div>
<div className="container">
  <div className="row">
    <div className="col-sm p-3">
   <div className="text-center">
    <div className="span-sm-16 span-md-16 span-lg-8 border-right">
      <img height="50px" width="53px" src="https://hrcdn.net/hackerrank/assets/home/icons/tech-371fcf2d367f8802a08628126af2145b.png" alt="Tech"/>
      <h5>DEVELOPERS</h5>
      <h2 className="p-3">Practice, Compete, Find Jobs</h2>
      <p className="p-3">The Test-Technique  Community is the largest learning
and competition community for programmers</p>
      <a href="/signup?utm_source=community_homepage&amp;utm_medium=middle_left&amp;utm_content=green_signup" className="btn btn-flat mlT btn-primary">Solve Challenge Now</a>
     <div className="p-5">    
      <div className="border span-sm-12 block-center mjB mjT pjT"></div>
</div>
      <div className="row">
        <div className="col-8 offset-md-2">
        <h4 className="text-center">I'm late to the party, but @Test-Technique  is addictive. Spent four hours in a row solving problems yesterday. #tired #coding #fun"</h4>
        <p className="text-center bold msT">- Marc Cataford on Twitter</p>
  </div>
  </div>
    </div>
      </div>
     </div>
     
    <div className="col-sm p-3">
      <div className="text-center ">
      <img height="50px" width="53px" src="https://hrcdn.net/hackerrank/assets/home/icons/developers-51c6f4fbcf4c1a9ebc3a4ea68b634600.png" alt="Developers"/>
      <h5>COMPANIES</h5>
      <h2 className="p-3">Assess, Screen, Interview</h2>
      <p className="p-3">Test-Technique for Work is the leading
 end-to-end technical recruiting platform for hiring engineers</p>
      <a href="/work?utm_source=community_homepage&amp;utm_medium=middle_right&amp;utm_content=green_signup" className="btn btn-flat mlT btn-primary">Learn more</a>
      <div className="p-5">
      <div className="border span-sm-12 block-center mjB pjT mjT"></div>
      </div>
      <blockquote className="pjT mjT">
      <div className="row">
        <div className="col-8 offset-md-2">
        <h4 className="txt-alt-grey text-center ">Test-Technique has allowed us to reach a wider, more diverse population of talent, as well as enhancing and streamlining our screening process.</h4>
       
        <p className="text-center bold msT">- Meagan Graham, Western Digital</p>
      </div>
      </div>
      </blockquote>
    </div>
    </div>
  </div>
</div>
	  <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
          <ModalHeader toggle={this.toggle}>INSCRIPTION Test -SQLI 2018</ModalHeader>
          <ModalBody>
            
          <Row className="justify-content-center">
            <Col md="12">
              <Card className="mx-4">
                <CardBody className="p-4">
                  <h1>Register</h1>
                  <p className="text-muted">Create your account</p>
                  
                <Alert color="primary">
                {
              ( signInError) ? (
                <h3>{ signInError}</h3>
              ) : (null)
            }
                </Alert>
         
                  <InputGroup className="mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="icon-user"></i>
                      </InputGroupText>
                    </InputGroupAddon>
                    
             <input 
              type="text"
              className="form-control"
              placeholder="Firstname" 
              value={signUpFirstName}
              onChange={this.onTextboxChangeSignUpFirstName}
              required
            />
             <input 
              type="text"
              className="form-control"
              placeholder="Lastname" 
              value={signUpLastName}
              onChange={this.onTextboxChangeSignUpLastName}
              required
            />
                  </InputGroup>
                  <InputGroup className="mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>@</InputGroupText>
                    </InputGroupAddon>
                        <input 
              type="email"
              className="form-control"
              placeholder="Email" 
              value={signUpEmail}
              onChange={this.onTextboxChangeSignUpEmail} 
              required autofocus
           />
                  </InputGroup>
            
                  <InputGroup className="mb-4">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="icon-lock"></i>
                      </InputGroupText>
                    </InputGroupAddon>
                      <Input type="select" value={signUpAgence} onChange= {this.onTextboxChangeSignUpAgence} required>
                        <option value="#">Please select Agence Sqli</option>
                         
                        <option value="Sqli oujda" >Sqli Oujda </option>
                        <option value="Sqli Rabat" >Sqli Rabat </option>
                                   
                      </Input>
                  </InputGroup>
                        <Button color="success" block onClick={this.onSignIn}>Create Account</Button>
                       
                </CardBody>
                <CardFooter className="p-4">
                  <Row>
                    <Col xs="12" sm="6">
                      <Button className="btn-facebook" block><span>facebook</span></Button>
                    </Col>
                    <Col xs="12" sm="6">
                      <Button className="btn-twitter" block><span>twitter</span></Button>
                    </Col>
                  </Row>
                </CardFooter>
              </Card>
            </Col>
          </Row>
          
         
         

            </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.toggle}>More Info </Button>{' '}
            <Button color="secondary" onClick={this.toggle}>Cancel</Button>
          </ModalFooter>
        </Modal>
     
        </div>
     
      );
    }

    return (
      <div>
         <Redirect to="/dashbord"/>
      </div>
    );
  }
}

export default Home;
