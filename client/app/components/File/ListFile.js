import React, { Component } from 'react';
import { Route, Redirect } from 'react-router';
import Layout from './../../layout'
import $ from 'jquery';
import DataTable from 'datatables.net';

import {
    Row,
    Col,
    Button,
    ButtonDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Card,
    CardHeader,
    CardFooter,
    CardBody,
    Collapse,
    Form,
    FormGroup,
    FormText,
    Label,
    Input,
    Table,
    InputGroup,
    InputGroupAddon,
    InputGroupText
  } from 'reactstrap';
import {
  BrowserRouter as Router,
  Link,
  Switch
} from 'react-router-dom'

import 'whatwg-fetch';
import axios from 'axios';

import {
  getFromStorage,
  setInStorage,
} from '../../utils/storage';

class EditQcm extends Component {

  constructor(props) {
    super(props);

    this.state = {
      qcms: {},
      files:[],
      posts:[],
      questions:[],
      reponse:[],
      Id:[],
      Question:'',
      Reponse:'',
      data:[],
    
    }; 
   
    
    
  }



      componentDidMount() {
   

      axios.get('/api/account/file')
      .then(res => {
        this.setState({ files: res.data });
     
      }); 
      axios.get('/api/account/post')
      .then(res => {
        this.setState({ posts: res.data });
 
      }); 
       
  }
 

 
  render() {
    
      $(document).ready( function () {
        setTimeout(function(){
          $('#ListFiles').DataTable();
        }, 400);
      });
     const { titre, description, duree,score,src, level, categorie,questions, Question,
      Reponse, 
    } = this.state;
    var cat =this.state.data;
    return (
    
     
       <div>
             
        <Row>  
           
          
<Col xs="12" md="12">
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> List File
              </CardHeader>
              <CardBody>
                <Table id="ListFiles" responsive bordered>
                  <thead>
                  <tr>

                  <th>From</th>
                  <th>Tests</th>
                  <th>Message</th>
                  
                  <th>Download</th>
                  <th>Operation</th>
                  </tr>
                  </thead>
                  <tbody>
                  {this.state.posts.map(post =>
              <tr key={post._id}>
                <th  scope="row"><Link to={`/users/showUser/${post._id}`} >{post.from}</Link></th> 


                <th>{post.to}</th>
                <th>{post.message}</th>
                <th> 
                <a  href={`http://localhost:3000/api/download/${post.files[0]}`}  className="btn btn-sm btn-primary"><i className="fa fa-pencil"></i> Download</a>
                </th>
                  <th>
                    <Link  to={`/users/showUser/${post._id}`}  className="btn btn-sm btn-primary"><i className="fa fa-pencil"></i> Show</Link>
                    <Link to={`/users/editUser/${post._id}`}  className="btn btn-sm btn-success"><i className="fa fa-pencil"></i> Edit</Link>
                    <Link to={`/users/deleteUser/${post._id}`}  className="btn btn-sm btn-danger"><i className="fa fa-pencil"></i> Delete</Link>
                    
                      </th>
              </tr>
            )}
                 
  
                  </tbody>
                </Table>
                 
              </CardBody>
            </Card>
          </Col>
          </Row>
      </div>

       
    );
  }
}

export default EditQcm;
