import React, { Component } from 'react';
import { Route, Redirect } from 'react-router';
import {
  BrowserRouter as Router,
  Link,
  Switch
} from 'react-router-dom'

import 'whatwg-fetch';
import axios from 'axios';
import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBody,
  Collapse,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Table,
} from 'reactstrap';
import {
  getFromStorage,
  setInStorage,
} from '../../utils/storage';

class Show extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      users: {}
      
    }; 
  }
  
      componentDidMount() {
    axios.get('/api/account/users/'+this.props.match.params.userId)
         
      .then(res => {
         console.log('eror');
        this.setState({ users: res.data });
    
      });
  }
  
   delete(userId){
    console.log(userId);
       
    axios.delete('/api/account/users/'+userId)
      .then((result) => {
        this.props.history.push("/users/listUser")
      });
  }

  render() {
    return (
      <div className="container">
 
     
         <Row>
          <Col>
          <Card>
              <CardHeader>
                <div className="text-center">
                <span> Information User  </span>
                  </div>
              </CardHeader>
              <CardBody className="text-center">
                <Form  className="form-horizontal">
                <FormGroup row>
                    <Col md="12">
                    <div className="text-center user-pic ">
                    <img src="https://image.freepik.com/free-icon/male-user-shadow_318-34042.jpg"/>
                    <span className="display-4">{this.state.users.prenom} | {this.state.users.nom} </span>
                   
                      
                       </div>
                        </Col>
                  </FormGroup>
                  </Form>
                  </CardBody>
                  </Card>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Details Information User
              </CardHeader>
              <CardBody>
                <Table hover bordered striped responsive size="sm">
                  <thead>
                  <tr>
                    <th>Nom</th>
                    <th>Prenom</th>
                    <th>Email</th>
                    <th>Role</th>
                  
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td>{this.state.users.nom}</td>
                    <td>{this.state.users.prenom}</td>
                    <td>{this.state.users.email}</td>
                    <td>{this.state.users.type}</td>
                    
                    
                   
                  </tr>
               
                  
               
                  </tbody>
                </Table>
                 
              </CardBody>
              <CardFooter>
              <div className="text-center">
                   <Link to={`/users/editUser/${this.state.users._id}`}  className="btn btn-sm btn-success"><i className="fa fa-pencil"></i> Edit</Link>
                    <Link to={`/users/deleteUser/${this.state.users._id}`}  className="btn btn-sm btn-danger"><i className="fa fa-pencil"></i> Delete</Link>
            </div>
                </CardFooter>
            </Card>
          </Col>
        </Row>
    </div>
    );
  }
}

export default Show;
