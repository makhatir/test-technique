import React, { Component } from 'react';
import { Route, Redirect } from 'react-router';
import {Container, Row, Col, CardGroup, Card, CardBody, Button, Input, InputGroup, InputGroupAddon, InputGroupText} from 'reactstrap';
import axios from 'axios';
import logo from './images/logo.png';
import {
  BrowserRouter as Router,
  Link,
  Switch
} from 'react-router-dom'
import {browserHistory } from 'react-router';
import { IndexRoute } from 'react-router';
import 'whatwg-fetch';

import {
  getFromStorage, 
  setInStorage,
} from '../../utils/storage';

class Auth extends Component {

 
  constructor(props) { 
    super(props);

    this.state = {
      isLoading: true,
      token: '',
      type: '',
      usersession: {} ,
      signUpError: '',
      signInError: '',
      signInEmail: '',
      signInPassword: '',
      signUpFirstName: '',
      signUpLastName: '',
      signUpEmail: '',
      signUpPassword: '',
    };

    this.onTextboxChangeSignInEmail = this.onTextboxChangeSignInEmail.bind(this);
    this.onTextboxChangeSignInPassword = this.onTextboxChangeSignInPassword.bind(this);
    this.onTextboxChangeSignUpEmail = this.onTextboxChangeSignUpEmail.bind(this);
    this.onTextboxChangeSignUpPassword = this.onTextboxChangeSignUpPassword.bind(this);
    this.onTextboxChangeSignUpFirstName = this.onTextboxChangeSignUpFirstName.bind(this);
    this.onTextboxChangeSignUpLastName = this.onTextboxChangeSignUpLastName.bind(this);
    
    this.onSignIn = this.onSignIn.bind(this);
    this.onSignUp = this.onSignUp.bind(this);
    this.logout = this.logout.bind(this);
 
  }

  componentDidMount() {
     
    const obj = getFromStorage('the_main_app');
      if(obj && obj.token){
            const {token} = obj;
           fetch('/api/account/signin')
           .then(res => res.json()) 
      }
  
    if (obj && obj.token) {
      const {token} = obj;
          
      //Verify token
      fetch('/api/account/verify?token=' + token)
    
      .then(res => res.json())
      .then(json => {
        if (json.success) {
          this.setState({
            token,
            isLoading: false,
          });
          axios.get('/api/account/usersession/'+ token)
          .then(res => {
            
            this.setState({ usersession: res.data });
            
         
           
          });
        } else {
          this.setState({
            isLoading: false,
          });
          console.log('eerrrr')
        }
      });
    } else {
      this.setState({
        isLoading: false,
      });
    }
  
  
  }
    

  onTextboxChangeSignInEmail(event) {
    this.setState({
      signInEmail: event.target.value,
    });
  }

  onTextboxChangeSignInPassword(event) {
    this.setState({
      signInPassword: event.target.value,
    });
  }

  onTextboxChangeSignUpFirstName(event) {
    this.setState({
      signUpFirstName: event.target.value,
    });
  }
  
  onTextboxChangeSignUpLastName(event) {
    this.setState({
      signUpLastName: event.target.value,
    });
  }

  onTextboxChangeSignUpEmail(event) {
    this.setState({
      signUpEmail: event.target.value,
    });
  }

  onTextboxChangeSignUpPassword(event) {
    this.setState({
      signUpPassword: event.target.value,
    });
  }

  onSignUp() {
    // Grab state
    const {
      signUpFirstName,
      signUpLastName,
      signUpEmail,
      signUpPassword,
    } = this.state;

    this.setState({
      isLoading: true,
    });
    // Post request to backend
    fetch('/api/account/signup', { 
      method: 'POST',
       headers: {
         'Content-Type': 'application/json'
       },
      body: JSON.stringify({
        nom: signUpFirstName,
        prenom: signUpLastName,
        email: signUpEmail,
        password: signUpPassword
      }),
      
    }).then(res => res.json())
      .then(json => {
        if(json.success){
          this.setState({
            signUpError: json.message,
            isLoading: false,
            signUpEmail: '',
            signUpPassword: '',
            signUpFirstName: '',
            signUpLastName:'',
          });
        } else {
          this.setState({
            signUpError: json.message,
            isLoading: false,
          });
        } 
      });
  }
  keydowns(event){
    console.log("keydown");
    if(event.keyCode == 13){
      this.onSignIn();
    }
  }
  onSignIn() {
    // Grab state
    const {
      signInEmail,
      signInPassword,
    } = this.state;

    this.setState({
      isLoading: true,
    });
    // Post request to backend
    fetch('/api/account/signin', { 
      method: 'POST',
       headers: {
         'Content-Type': 'application/json'
       },
      body: JSON.stringify({
        email: signInEmail,
        password: signInPassword
      }),
      
    }).then(res => res.json())
      .then(json => {
        if(json.success){
          setInStorage('the_main_app', { token: json.token});
          this.setState({
            signInError: json.message,
            isLoading: false,
            signInEmail: '',
            signInPassword: '',
            token: json.token,
                      });
                      
        } else {
          this.setState({
            signInError: json.message,
            isLoading: false,
          });
        } 
      });
      

  }

  logout() {
    this.setState({
      isLoading: true
    })
    const obj = getFromStorage('the_main_app');
    if (obj && obj.token) {
      const { token } = obj;
      //Verify token
      fetch('/api/account/logout?token=' + token)
      .then(res => res.json())
      .then(json => {
        if (json.success) {
          this.setState({
            token:'',
            isLoading: false,
             
              
          });
             window.location = "/";
        } else {
          this.setState({
            isLoading: false,
          });
          console.log('eerrrr')
        }
      });
    } else {
      this.setState({
        isLoading: false,
      });
    }
  }
 
  render() {
    const {
      isLoading,
      token,
      type,
      usersession: {} ,
      signInError,
      signInEmail,
      signInPassword,
      signUpFirstName,
      signUpLastName,
      signUpEmail,
      signUpPassword,
      signUpError 
    
        
      
    } = this.state;

    if (isLoading) {
      return(<div><p>Loading...</p></div>);
    }
    if (!token) {
      return (
        <div>
<div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody onKeyDown={this.keydowns(event)}>
                    <h1>Login </h1>
                    <p className="text-muted"> Please Sign In to your account</p>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-user"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                        <input 
                  type="email"
                  className="form-control"
                  placeholder="Email" 
                  value={signInEmail}
                  onChange={this.onTextboxChangeSignInEmail}
                  required autofocus
                  
              />
                    </InputGroup>
                    <InputGroup className="mb-4">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-lock"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                           <input 
                  type="password"
                  className="form-control"
                  placeholder="Password" 
                  value={signInPassword} 
                  onChange={this.onTextboxChangeSignInPassword}
                  required
              />
                    </InputGroup>
                    <Row>
                      <Col xs="6">
                        <Button color="primary" onClick={this.onSignIn} className="px-4">Login</Button>
                      </Col>
                      <Col xs="6" className="text-right">
                        <Button color="link" className="px-0">Forgot password?</Button>
                      </Col>
                    </Row>
                  </CardBody>
                </Card>
                <Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: 44 + '%' }}>
                  <CardBody className="text-center">
                    <div>
                      <h2>Sign up</h2>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua.</p>
                      <Button color="primary" className="mt-3" active href="/Register">Register Now!</Button>
                    </div>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
        </div>
      );
    }

    
    return (
    
      <div>
       
       
        <Redirect push to="/home"/>
        
       
      </div>
    );
 
  }

}

export default Auth;





