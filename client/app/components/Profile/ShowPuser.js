import React, { Component } from 'react';
import { Route, Redirect } from 'react-router';
import {
  BrowserRouter as Router,
  Link,
  Switch
} from 'react-router-dom'

import 'whatwg-fetch';
import axios from 'axios';
import $ from 'jquery';
import DataTable from 'datatables.net';
import {
  getFromStorage,
  setInStorage,
} from '../../utils/storage';
import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBody,
  Collapse,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Table,
  Jumbotron
} from 'reactstrap';

class ShowP extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      collab: {},
      qcms:[],
         
  
    };

 
     
   
  }

      componentDidMount() {
        axios.get('/api/account/collabs/'+this.props.match.params._id)
         
      .then(res => {
         console.log('eror');
        this.setState({ collab: res.data });
        console.log( this.state.collab.length);
         
      });
      axios.get('/api/account/qcm')
      .then(res => {
        this.setState({ qcms: res.data });
        console.log(this.state.qcms);
      }); 
     
    
  }
    

  
 
  render() {
    
      $(document).ready( function () {
      setTimeout(function(){
        $('#QcmP').DataTable();
      }, 400);
    });


  if (!this.state.collab._id) {
      return (
         <div>

         </div>
 

       
      );
    }
   
    if (this.state.collab._id) {
     
  
    return (
    
       <div className="container">

        <Row>
         
          <Col>
                <Card>
                <CardHeader>
                <i className="fa fa-align-justify"></i> Test-Technique
              </CardHeader>
              <CardBody>
            
        
                  
                  {this.state.qcms.map(qcm =>
                  {
                    if(qcm._id == this.state.collab.testP)
                 return( 
                 
                   
                  <Jumbotron>
                  
                    
              
              <h1 className="display-3 text-center">Projet...{qcm.titre}</h1>
              <p className="lead text-center">{qcm.description}</p>
              <hr className="my-2"/>
              <p className="text-center"> Level is <span>{qcm.level}</span> </p>
              <div className="text-center">
            <span className="">
            <i className="fa fa-clock-o fa-lg mt-4"> {qcm.duree}  Min</i> 
                    </span>
            </div>
              <p className="text-center ml-1"> 
          <Link to={`/appQcm/${qcm._id}`} color="primary">
                <Button  color="primary" >
                     
                    Launch Test 
                    </Button></Link>

            
             
              </p>
             
             
            </Jumbotron>
                  
                  ); }
                )}
                
              
                 
                 
              </CardBody>
            </Card>
          </Col>
        </Row>
 
            </div> 
      

       
    );
  }
  }
}

export default ShowP;
