import React, { Component } from 'react';
import { Route, Redirect } from 'react-router';
import {
  BrowserRouter as Router,
  Link,
  Switch
} from 'react-router-dom'

import 'whatwg-fetch';
import axios from 'axios';

import {
  getFromStorage,
  setInStorage,
} from '../../utils/storage';
import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBody,
  Collapse,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Table,
} from 'reactstrap';

class ShowP extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      users: {},
      qcms:[],
      categories:[]
  
    };

 
     
   
  }

      componentDidMount() {
    axios.get('/api/account/users/'+this.props.match.params.userId)
         
      .then(res => {
         console.log('eror');
        this.setState({ users: res.data });
        console.log(this.state.users);
        console.log('sucss',this.state.users.nom);
      });

      axios.get('/api/account/qcm')
      .then(res => {
        this.setState({ qcms: res.data });

        console.log(this.state.qcms);
      }); 
     
      axios.get('/api/account/categories')
      .then(res => {
        this.setState({ categories: res.data });

      
      }); 
  }
    

  
 
  render() {
    
 
 
  if (!this.state.users.userId) {
      return (
         <div>

         </div>
 

       
      );
    }
   
    if (this.state.users.userId) {
     
  
    return (
    
       <div className="container">

        <Row>
          <Col  md="12">
          <Card>
              <CardHeader>
                <div className="text-center">
                <span> Information User  </span>
                  </div>
              </CardHeader>
              <CardBody className="text-center">
                <Form  className="form-horizontal">
                <FormGroup row>
                    <Col md="12">
                    <div className="text-center user-pic ">
                    <img src="https://image.freepik.com/free-icon/male-user-shadow_318-34042.jpg"/>
                    <span className="display-4">{this.state.users.prenom} | {this.state.users.nom} </span>
                   
                      
                       </div>
                        </Col>
                  </FormGroup>
                  </Form>
                  </CardBody>
                  </Card>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Your information personnel
              </CardHeader>
              <CardBody>
                <Table hover bordered striped responsive size="sm">
                  <thead>
                  <tr>
                    <th>Nom</th>
                    <th>Prenom</th>
                    <th>Email</th>
                    <th>Role</th>
                  
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td>{this.state.users.nom}</td>
                    <td>{this.state.users.prenom}</td>
                    <td>{this.state.users.email}</td>
                    <td>{this.state.users.type}</td>
                    
                    
                   
                  </tr>
               
                  
               
                  </tbody>
                </Table>
                 
              </CardBody>
           <CardFooter>
           <Link to={`/EditProfile/${this.state.users._id}`}  className="btn btn-sm btn-success"><i className="fa fa-pencil"></i> Edit</Link>
                   
           </CardFooter>
            </Card>
          </Col>
          <Col  md="12">
                <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Test Créer
              </CardHeader>
              <CardBody>
              <Table id="ListQcms" className="display" hover responsive className="table-outline mb-0 d-none d-sm-table">
                  <thead>
                  <tr>
                    <th>Titre</th>
                    <th>Description</th>
                    <th>Duree</th>
                    <th>Score</th>
                    <th>Level</th>
                    <th>Auteur</th>
                    <th>Categorie</th>
                    <th> Update</th>
                     <th>Operation</th>
                  </tr>
                  </thead>
                  <tbody>
                  {this.state.qcms.map(qcm =>
                  {
                    if(qcm.auteur == this.state.users.email)
                 return( 
                  <tr>
                    <td scope="row"><Link to={`/showQcm/${qcm._id}`} >{qcm.titre}</Link></td> 
                    <td>{qcm.description}</td>
                    <td>{qcm.duree}</td>
                    <td>{qcm.score}</td>
                    <td>{qcm.level}</td>
                    <td>{qcm.auteur}</td>
                    <td>{qcm.categorie}</td>
                  
                    <td> <Link to={`/addQuestion/${qcm._id}`} >Add Question</Link></td>
                     <td><Link to={`/deleteQcm/${qcm._id}`}><button  className="btn btn-danger">Delete</button></Link>
            <Link to={`/editQcm/${qcm._id}`}  className="btn btn-success">Edit</Link>

</td>
                  </tr>
                  ); }
                )}
                
                  </tbody>
                </Table>
                 
                 
              </CardBody>
           <CardFooter>
           <Link to={`/EditProfile/${this.state.users._id}`}  className="btn btn-sm btn-success"><i className="fa fa-pencil"></i> Edit</Link>
                   
           </CardFooter>
            </Card>
          </Col>
        </Row>
        <Col>
<Card>
<CardHeader>
  <i className="fa fa-align-justify"></i> File Upload
</CardHeader>
<CardBody>
  <Table hover bordered striped responsive size="sm">
    <thead>
    <tr>
                <th>Titre</th>
                  <th>Description</th>
                  <th>Operation</th>
    
    </tr>
    </thead>
    <tbody>    
    { this.state.categories.map(info =>
                  {
                    if(info.username==this.state.users.nom)
                 return( 
                  <tr>
                    <td scope="row"><Link to={`/showQcm/${info._id}`} >{info.titre}</Link></td> 
                    <td>{info.description}</td>
             
                    
                   
                  
                       <td><Link to={`/deleteCtg/${info._id}`}><button  className="btn btn-danger">Delete</button></Link>
            <Link to={`/editCtg/${info._id}`}  className="btn btn-success">Edit</Link>

</td>
                  </tr>
                  ); }
                )}
 
    
 
    </tbody>
  </Table>
   
</CardBody>
<CardFooter>
     
</CardFooter>
</Card>
</Col>
            </div> 
      

       
    );
  }
  }
}

export default ShowP;
