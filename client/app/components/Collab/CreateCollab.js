import React, { Component } from 'react';
import { Route, Redirect } from 'react-router';
import ListCollab from './ListCollab';
import axios from 'axios';
import 'whatwg-fetch';
import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBody,
  Collapse,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText
} from 'reactstrap';
import {
  getFromStorage,
  setInStorage,
} from '../../utils/storage';

class CreateCtg extends Component {
    constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      token: '',
      addCatgError: '',
      qcms:[],
      nom: '',
      prenom: '',
      email:'',
      testP: '',
      usersession: {} ,
      
   
    };

    this.onTextboxChangeNom = this.onTextboxChangeNom.bind(this);
    this.onTextboxChangePrenom = this.onTextboxChangePrenom.bind(this);
    this.onTextboxChangeEmail = this.onTextboxChangeEmail.bind(this);
    this.onTextboxChangeTestP = this.onTextboxChangeTestP.bind(this);
  
    this.addCollab = this.addCollab.bind(this);
   
  }
  componentDidMount() {

    axios.get('/api/account/qcm')
    .then(res => {
      this.setState({ qcms: res.data });
      console.log(this.state.qcms);
    });
    const obj = getFromStorage('the_main_app');
      if(obj && obj.token){
            const {token} = obj;
           fetch('/api/account/signin')
           .then(res => res.json()) 
      }
  
    if (obj && obj.token) {
      const {token} = obj;
          
      //Verify token
      fetch('/api/account/verify?token=' + token)
    
      .then(res => res.json())
      .then(json => {
        if (json.success) {
          this.setState({
            token,
            isLoading: false,
          });
          axios.get('/api/account/usersession/'+ token)
          .then(res => {
           
            this.setState({ usersession: res.data });
           
          });
        } else {
          this.setState({
            isLoading: false,
          });
        
        }
      });
    } else {
      this.setState({
        isLoading: false,
      });
    }
    
  
  }
    

  onTextboxChangeNom(event) {
    this.setState({
      nom: event.target.value,
    });
  }
  onTextboxChangePrenom(event) {
    this.setState({
      prenom: event.target.value,
    });
  }
  onTextboxChangeEmail(event) {
    this.setState({
      email: event.target.value,
    });
  }
  
  onTextboxChangeTestP(event) {
    this.setState({
      testP: event.target.value,
    });
  }


   
  addCollab() {
    // Grab state
    const {
      nom,
      prenom,
      email,
      testP,
     

    
    
           } = this.state;

    this.setState({
      isLoading: true,
    });
    // Post request to backend
    fetch('/api/account/collab', { 
      method: 'POST',
       headers: {
         'Content-Type': 'application/json'
       },
      body: JSON.stringify({
        nom: nom,
        prenom: prenom,
        email:email,
        testP:testP,
       
       
      }),
      
    }).then(res => res.json())
      .then(json => {
        if(json.success){
          this.setState({
            addQcmError: json.message,
            isLoading: false,
            nom: '',
            prenom:'',
            email:'',
            testP:'',
          });
        } else {
          this.setState({
            addQcmError: json.message,
            isLoading: false,
          });
        } 
      });
  }


 

 
  render() {
    const {
      isLoading,
      token,
      nom,
      prenom,
      email,
      testP,
      
      
    } = this.state;

    if (isLoading) {
      return(<div><p>Loading...</p></div>);
    }
    if (!token) {
      return (
        <div>
   <Redirect to="/login"/>

        </div>
      );
    }

    return (
 
 <Row>
          <Col xs="12" md="6">
            <Card>
              <CardHeader>
                <strong>Add New Invite</strong>
           
              </CardHeader>
              <CardBody>
                <Form className="form-horizontal">
                 
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Titre</Label>
                    </Col>
                    <Col xs="12" md="9">
                <Input type="text" name="titre" value={nom} onChange={this.onTextboxChangeNom} required placeholder="Nom" />
                      
                     
                    </Col>
                  </FormGroup>
                     <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Prenom</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" name="prenom" value={prenom} onChange={this.onTextboxChangePrenom} required rows="9"
                             placeholder="prenom..."/>
                    </Col>

                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Email</Label>
                    </Col>
                    <Col xs="12" md="9">
                <Input type="text" name="email" value={email} onChange={this.onTextboxChangeEmail} required placeholder="Email" />
                      
                      <FormText color="muted">Entrer un email n'existe pas </FormText>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
         
                      <Label htmlFor="select"> Test </Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="select" value={testP} onChange= {this.onTextboxChangeTestP} required>
                        <option value="#">Please select Test</option>
                        {this.state.qcms.map(qcm =>
                        <option value={qcm._id} > {qcm.titre}</option>
                                    )}
                      </Input>
                    </Col>
                  </FormGroup>
                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" onClick={this.addCollab}><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Reset</Button>
              </CardFooter>
            </Card>
            
          </Col>
           <Col xs="12" md="6">
            <ListCollab />
          </Col>
        </Row>
 

    );
  }
}

export default CreateCtg;
