import React, { Component } from 'react';
import { Route, Redirect } from 'react-router';
import {
  Badge,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink
} from 'reactstrap';

import {
  BrowserRouter as Router,
  Link,
  Switch
} from 'react-router-dom'

import 'whatwg-fetch';
import axios from 'axios';
 
import {
  getFromStorage,
  setInStorage,
} from '../../utils/storage';
import $ from 'jquery';
import DataTable from 'datatables.net';
class ShowCtg extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      collab: {},
      data:[]
 
  
    };
 
  }

  componentDidMount() {
      
    axios.get('/api/account/collabs/'+this.props.match.params._id)
         
      .then(res => {
         console.log('eror');
        this.setState({ collab: res.data });
        console.log( this.state.collab.length);
 
 
      
         
      });

 
  }
    
  
 
    

 
  
 
  render() {
    $(document).ready( function () {
      setTimeout(function(){
        $('#Listcollab').DataTable();
      }, 400);
    });
 
    return (
    
 
        
       <div className="container">
       <Row>
          <Col>
         
            <Card>
              <CardHeader>
             
           

               
                <i className="fa fa-align-justify"></i> Collab:  {this.state.collab.nom}  
                <span className="text-center">  <Link to="/listcollab" className="btn btn-success">
                       List Collab</Link></span>
              </CardHeader>
              <CardBody>
                <Table hover bordered striped responsive size="sm">
                  <thead>
                  <tr>
                    <th>Nom</th>
                    <th>Prenom</th>
                    <th> Email </th>
                    <th> Operation </th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td>{this.state.collab.nom}</td>
                    <td>{this.state.collab.prenom}</td>
                    <td>{this.state.collab.email}</td>
                    <td>
        <Link to={`/editCollab/${this.state.collab._id}`}  
        className="btn btn-warning">
       Edit Collab
                      </Link>
                  </td>
            
                  </tr>
                 
                  </tbody>
                  
                </Table>
                 
              </CardBody>
            </Card>
          </Col>
        
        </Row>
         

  
          </div> 
      

       
    );
  }
}

export default ShowCtg;
