import React, { Component } from 'react';
import { Route, Redirect } from 'react-router';
import {
  Badge,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink
} from 'reactstrap';
import {
  BrowserRouter as Router,
  Link,
  Switch
} from 'react-router-dom'

import 'whatwg-fetch';
import axios from 'axios';

import {
  getFromStorage,
  setInStorage,
} from '../../utils/storage';
import $ from 'jquery';
import DataTable from 'datatables.net';
class ListCollab extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      collab: [],
   
      
  
    };

 
     
 
  }
 
  componentDidMount() {
    axios.get('/api/account/collab')
      .then(res => {
        this.setState({ collab: res.data });
        console.log("collab",this.state.collab);
      
      }); 
      
    
  }
 

 
 
  render() {
    $(document).ready( function () {
      setTimeout(function(){
        $('#ListCollab').DataTable();
      }, 400);
    });



   
   const cat = this.state.collab;
    return (
    
      <div>
            

<Col xs="12" lg="12">
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> list Invite
              </CardHeader>
              <CardBody>
                <Table id="ListCollab" responsive bordered>
                  <thead>
                  <tr>
                  <th>Nom</th>
                  <th>Prenom</th>
                  <th>Email</th>
                  <th>Operation</th>
                  </tr>
                  </thead>
                  <tbody>
                  {this.state.collab.map(collab =>
                  <tr>
         <th scope="row"><Link to={`/ShowCollab/${collab._id}`} >{collab.nom}</Link></th>
                    <td>{collab.prenom}</td>
                    <td>{collab.email}</td>
                    
                     <td>   <Link to={`/deleteCollab/${collab._id}`}  ><button  className="btn btn-danger">Delete</button></Link>
            <Link to={`/editCollab/${collab._id}`}  className="btn btn-success">Edit</Link>

</td>
                  </tr>
                )}
                  
                  </tbody>
                </Table>
                 
              </CardBody>
            </Card>
          </Col>

         
         
    
         
      </div>
    );
  }
}

export default ListCollab;
