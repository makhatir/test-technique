 import React, { Component } from 'react';
import { Route, Redirect } from 'react-router';
 
import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBody,
  Collapse,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink
} from 'reactstrap';

import {
  BrowserRouter as Router,
  Link,
  Switch
} from 'react-router-dom'

import 'whatwg-fetch';
import axios from 'axios';
import {
  getFromStorage,
  setInStorage,
} from '../../utils/storage';
import $ from 'jquery';
import DataTable from 'datatables.net';
class EditCollab extends Component {
  constructor(props) {
    super(props);

    this.state = {
      collab: {},
      qcms:[],
      nom:'',
      prenom:'',
      email:'',
      testP:'',

    }; 
    this.onTextboxChangeNom = this.onTextboxChangeNom.bind(this);
    this.onTextboxChangePrenom = this.onTextboxChangePrenom.bind(this);
    this.onTextboxChangeEmail = this.onTextboxChangeEmail.bind(this);
    this.onTextboxChangeTestP = this.onTextboxChangeTestP.bind(this);
       
     this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  
     
  }    
      onChange(event) {
          const state = this.state.collab;
    state[event.target.name] = event.target.value;
    this.setState({value: event.target.value});
  }


  onTextboxChangeNom(event) {
    this.setState({
      nom: event.target.value,
    });
  }
  onTextboxChangePrenom(event) {
    this.setState({
      prenom: event.target.value,
    });
  }
  onTextboxChangeEmail(event) {
    this.setState({
      email: event.target.value,
    });
  }
  onTextboxChangeTestP(event) {
    this.setState({
      testP: event.target.value,
    });
  }
  onSubmit(event) {
    alert(' Collab : ' + this.state.collab.nom + ' is update');
    event.preventDefault();
          const { nom, 
            prenom, 
            email,
            testP,
          } = this.state.collab;
       

    axios.put('/api/account/collabs/'+this.props.match.params._id, {nom, prenom,email,testP})
      .then((result) => {
       
        this.props.history.push("/showCollab/"+this.props.match.params._id)
      
      });
  }
 
 

      componentDidMount() {
      
    axios.get('/api/account/collabs/'+this.props.match.params._id)
         
      .then(res => {
        this.setState({ collab: res.data });
        console.log('edit',this.state.collab.nom);
     
      
         
      });

      axios.get('/api/account/qcm')
      .then(res => {
        this.setState({ qcms: res.data });
        console.log(this.state.qcms);
      });

 
  }
 
  
 
  render() {

    $(document).ready( function () {
      setTimeout(function(){
        $('#ListCollab').DataTable();
      }, 400);
    });
     const {
      nom,
      prenom,
      email,
      testP,
    } = this.state;
  

 
    return (
    
   
       <div>
      
             
   

     
               
                         
              
         
         <Row>
        
           
          <Col xs="12" md="12">
          <Card>
          <CardHeader>
          <strong>Edit Collab</strong>
          <Link to={`/showCollab/${this.state.collab._id}`}> / Show Collab </Link>
          </CardHeader>
              <CardBody>
          <Form  className="form-horizontal">
          <FormGroup row>
                <Col md="3">
                <Label for="nom">Nom:</Label>
                </Col>
                <Col xs="12" md="9">
                <Input type="text" name="nom" value={this.state.collab.nom} onChange={this.onChange} placeholder="nom" />
                <FormText color="muted">Enter name categorie n'existe pas </FormText>
                </Col>
                </FormGroup>
                <FormGroup row>
                <Col md="3">
                 <Label for="prenom">Prenom:</Label>
                 </Col>
                 <Col xs="12" md="9">
                    <Input type="text" name="prenom" value={this.state.collab.prenom} onChange={this.onChange} required rows="9"
                             placeholder="prenom..."/>
                </Col>
                </FormGroup>
                <FormGroup row>
                <Col md="3">
                 <Label for="email">Email:</Label>
                 </Col>
                 <Col xs="12" md="9">
                    <Input type="text" name="email" value={this.state.collab.email} onChange={this.onChange} required rows="9"
                             placeholder="email..."/>
                </Col>
                </FormGroup>

                   <FormGroup row>
                    <Col md="3">
         
                      <Label htmlFor="select"> Test </Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="select" name="testP" value={this.state.collab.testP} onChange= {this.onChange} required>
                        <option value="">Aucun Test</option>
                        {this.state.qcms.map(qcm =>
                        <option value={qcm._id} > {qcm.titre}</option>
                                    )}
                      </Input>
                    </Col>
                  </FormGroup>
               
               </Form>
               </CardBody>
              <CardFooter>
              <div className="text-center"> <Button type="submit" onClick={this.onSubmit} className="btn btn-danger">update</Button>
             </div> </CardFooter> </Card>
          </Col>
          
               
        </Row>
 
      </div>

       
    );
  }
}

export default EditCollab;
