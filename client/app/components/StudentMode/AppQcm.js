import React, {Component} from 'react';
import ReactCountdownClock from 'react-countdown-clock';
import axios from 'axios';
 
 
import {
    BrowserRouter as Router,
    Link,
    Switch, Route, Redirect
  } from 'react-router-dom'
  
  import 'whatwg-fetch';
import {
    getFromStorage,
    setInStorage,
  } from '../../utils/storage';
  import Layout from './../../layout2'
import {
    Row,
    Col,
    Button,
    ButtonDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Card,
    CardHeader,
    CardFooter,
    CardBody,
    Collapse,
    Form,
    FormGroup,
    FormText,
    Label,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Modal, ModalHeader, ModalBody, ModalFooter,
    Jumbotron, Container
  } from 'reactstrap';
class AppQcm extends Component{
  constructor(props) {
    super(props);
    this.state = {
      score: 0,
      current: 1,
      qcms:{},
      questions:[],
       
    }
      this.logout = this.logout.bind(this);
    
  }

 

    logout() {
        this.setState({
            isLoading: true
        })
        const obj = getFromStorage('the_main_app');
        if (obj && obj.token) {
            const { token } = obj;
            //Verify token
            fetch('/api/account/logout?token=' + token)
                .then(res => res.json())
                .then(json => {
                    if (json.success) {
                        this.setState({
                            token:'',
                            isLoading: false,


                        });
                        window.location = "/";
                    } else {
                        this.setState({
                            isLoading: false,
                        });
                        console.log('eerrrr')
                    }
                });
        } else {
            this.setState({
                isLoading: false,
            });
        }
    }
  componentDidMount() {
    axios.get('/api/account/qcms/'+this.props.match.params._id)
         
      .then(res => {
         console.log('eror');
        this.setState({ qcms: res.data });

        console.log(this.state.qcms['questions']);
        this.setState({
            questions: this.state.qcms['questions']
           
        })
     
 
    
      });
      window.onunload = function () {
        this.logout();
    }
  }

  render() {
 
    return(
      <div>
      <Row>
    
          <Col xs="12" lg="9">
          <Card>
         
              <CardBody>
          <FormGroup row>
         
          <Col xs="12" md="12">
          
          <Layout />
          </Col>
        </FormGroup>
        </CardBody>
        <CardFooter>

        </CardFooter>
        </Card>
        </Col>
        <Col  xs="12 " lg="3">
        
        <Card>
        <CardBody>
            
         
       
     <h2 className="display-3 text-center">  <div className="logo-sqli  p-3"></div></h2>
   
        </CardBody>
        <CardFooter>
            <i className="fa fa-align-justify"></i><strong>
         
                 </strong>
                 <h1 className="display-4 text-center">{this.state.qcms.titre}</h1>
              <p className="lead text-center">{this.state.qcms.description}</p>
              <hr className="my-2"/>
              <p className="text-center"> Level is <span>{this.state.qcms.level}</span> </p>
              <div className="text-center">
      
            <i className="fa fa-clock-o fa-lg mt-4"> {this.state.qcms.duree}  Min</i> 

            
            </div>
                
          </CardFooter>
          <CardHeader>
                <Row>
                <Col>
                <div className="clock-app">
        <ReactCountdownClock seconds={this.state.qcms.duree}
                     color="#000"
                     alpha={0.9}
                     size={100}
                     onComplete={this.logout} />
                   </div> </Col></Row> </CardHeader>
        </Card>
        </Col>

      </Row>
      </div>
    )
  }
}

module.exports = AppQcm;
