import React, {Component} from 'react';
import Auth from '../Auth/Auth';
import PropTypes from 'prop-types';
import axios from 'axios';
import 'whatwg-fetch';
import {
  BrowserRouter as Router,
  Link,
  Switch
} from 'react-router-dom';
import {
  getFromStorage,
  setInStorage,
} from '../../utils/storage';
import {
  Nav,
  NavbarBrand,
  NavbarToggler,
  NavItem,
  NavLink,
  Badge,
  TabPane,
  Popover, PopoverHeader, PopoverBody ,Button,DropdownToggle,Dropdown
} from 'reactstrap';
import HeaderDropdown from './HeaderDropdown';
import Notification from './Notification';
class Header extends Auth {

  constructor(props) {
    super(props);
      this.toggle = this.toggle.bind(this);
     
    this.state = {
      isLoading: false,
      users: [],
      qcms:[],
      popoverOpen: false
    };
  }

  sidebarToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-hidden');
  }

  sidebarMinimize(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-minimized');
  }

  mobileSidebarToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-mobile-show');
  }

  asideToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('aside-menu-hidden');
  }
   toggle() {
    this.setState({
      popoverOpen: !this.state.popoverOpen
    });
  }
 
 
  
   
  

    Qcms (){
       axios.get('/api/account/qcm')
    .then(res => {
      this.setState({ qcms: res.data });
    
      
    
    }); 
    if(!this.state.qcms) return;
  
    return (<div>
  
        <TabPane tabId="1">
          {this.state.qcms.map((qcm,i) =>
          {
            if(i>this.state.qcms.length-4)
            return(  

        
            <div className="message">
              <div className="py-3 pb-5 mr-3 float-left">
                <div className="avatar">
                  <img src={'https://cdn3.iconfinder.com/data/icons/brain-games/1042/Puzzle.png'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                  <span className="avatar-status badge-success"></span>
                </div>
              </div>
        
              
              
              
                <small className="text-muted">Created By {qcm.auteur}</small>
                
              
              <div className="text-truncate font-weight-bold">  <Link to={`/showQcm/${qcm._id}`} > {qcm.titre}</Link></div>
   

              <small className="text-muted"> {qcm.description} </small>
             
            

            <hr/>

            </div> 
            );    
          }
          )}
                                     <div className="text-center"> <Link  to={`addQcm/qcms/`}  className="btn btn-sm btn-primary"><i className="fa fa-pencil"></i> Show All </Link></div>
          </TabPane>
         
    </div>);
  }
  render() {
    const {
      isLoading,
      token,
     users: [],
    } = this.state;

    if (isLoading) {
      return(<div><p>Loading...</p></div>);
    }
    if (!token) {
      return (
        <div>
   <header className="app-header navbar">
        <NavbarToggler className="d-lg-none" onClick={this.mobileSidebarToggle}>
          <span className="navbar-toggler-icon"></span>
        </NavbarToggler>
       <a href="/#/" className="title-log"><img src={'img/qcm.png'} className="logo-navbar"/></a>
         
           <Nav className=" ml-auto" navbar>
          <NavItem className="px-3">
            <NavLink href="/#/"><Button outline color="secondary" >Home </Button></NavLink>
          </NavItem>
           <NavItem className="px-3">
            <NavLink href="/#/"> <Button outline color="success" >Contact Us</Button> </NavLink>
          </NavItem>
        
        
         
        </Nav>
      </header>
 
                
 
   
   
        </div>
      );
    }

    return (
      <div>
     <header className="app-header navbar">
        <NavbarToggler className="d-lg-none" onClick={this.mobileSidebarToggle}>
          <span className="navbar-toggler-icon"></span>
        </NavbarToggler>
         
        <a href="/#/home" className="title-log"><img src={'img/qcm.png'} className="logo-navbar"/></a>
        <NavbarToggler className="d-md-down-none" onClick={this.sidebarToggle}>
          <span className="navbar-toggler-icon"></span>
        </NavbarToggler>
        <Nav className="d-md-down-none" navbar>
          <NavItem className="px-3">
            <NavLink href="/#/dashboard">Dashboard</NavLink>
          </NavItem>
          <NavItem className="px-3">
            <NavLink href="/#/users/listUser">Users</NavLink>
          </NavItem>
                 </Nav>
        <Nav className="ml-auto" navbar>
          <NavItem className="d-md-down-none">
            
          <div>
            <Dropdown nav isOpen={this.state.popoverOpen} toggle={this.toggle}>
           <DropdownToggle nav >
        <i className="icon-bell" id="Popover1" onClick={this.toggle} ></i><Badge  pill color="danger">{this.state.qcms.length}</Badge> 
        </DropdownToggle>
        <Popover placement="bottom" isOpen={this.state.popoverOpen} target="Popover1" toggle={this.toggle}>
          <PopoverHeader>TEST Added </PopoverHeader>
          <PopoverBody>
       
    {this.Qcms()}
             
       
          
          </PopoverBody>
        </Popover>
        </Dropdown>
      </div>
          </NavItem>
          <NavItem className="d-md-down-none">
            <NavLink href="#"><i className="icon-list"></i></NavLink>
          </NavItem>
          <NavItem className="d-md-down-none">
            <NavLink href="#"><i className="icon-location-pin"></i></NavLink>
          </NavItem>
          <HeaderDropdown/>
        </Nav>
        <NavbarToggler className="d-md-down-none" onClick={this.asideToggle}>
          <span className="navbar-toggler-icon"></span>
        </NavbarToggler>
      </header>
      </div>
    );
  }
}

export default Header;

      