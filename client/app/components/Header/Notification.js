
import React, { Component } from 'react';
import { Route, Redirect } from 'react-router';
 
import {
  Badge,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Dropdown,
  Card,
  CardHeader,
  CardFooter,
  CardBody,
  Col,
  Table,
  TabPane,
  TabContent
} from 'reactstrap';
import {
  BrowserRouter as Router,
  Link,
  Switch
} from 'react-router-dom'
import axios from 'axios';
import 'whatwg-fetch';
import {
  getFromStorage,
  setInStorage,
} from '../../utils/storage';
class Notif extends Component {

  constructor(props) {
    super(props);
    this.state = {
        qcms:[],
      
      };

    this.toggle = this.toggle.bind(this);
    this.state = {
      dropdownOpen: false
    };
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }
  componentDidMount() {
 
  
    axios.get('/api/account/qcm')
    .then(res => {
      this.setState({ qcms: res.data });
      this.state.qcms.map(info=>
        console.log(info.titre)
    )
      console.log('--------not',this.state.qcms.length)
    
    }); 
  }
 handleScrollToElement(event) {
  const tesNode = ReactDOM.findDOMNode(this.refs.test)
  if (some_logic){
    window.scrollTo(0, tesNode.offsetTop);
  }
}
  Qcms (){
    if(!this.state.qcms) return;
  
    return (<div>
  
        <TabPane tabId="1">
          {this.state.qcms.map(qcm =>
            <div className="message">
              <div className="py-3 pb-5 mr-3 float-left">
                <div className="avatar">
                  <img src={'https://cdn3.iconfinder.com/data/icons/brain-games/1042/Puzzle.png'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                  <span className="avatar-status badge-success"></span>
                </div>
              </div>
        
              
              
              
                <small className="text-muted">Created By {qcm.auteur}</small>
                <small className="text-muted float-right mt-1">{qcm.categorie}</small>
              
              <div className="text-truncate font-weight-bold"> {qcm.titre}</div>
              <small className="text-muted"> {qcm.description} </small>
             
            

            <hr/>
            </div> 
          )}
             
          </TabPane>
         
    </div>);
  }
  dropAccnt() {
    return (
     
      <Dropdown nav isOpen={this.state.dropdownOpen} toggle={this.toggle}>
        <DropdownToggle nav>
        <i className="icon-bell"></i><Badge pill color="danger">{this.state.qcms.length}</Badge> 
        </DropdownToggle>
      
        <DropdownMenu right>
        
           
        {this.Qcms()}
      

        </DropdownMenu>
      </Dropdown>
    );
  }

  render() {
    const {attributes  } = this.props;
   
    return (
        this.dropAccnt()
      
    );
  }
}

export default Notif;
