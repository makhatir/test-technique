const User = require('./../models/User');
var mongoose = require('mongoose'), user = mongoose.model('Users');
 
exports.list_all_users = function (req, res) {
  User.find({}, function (err, user) {
     
    if (err) res.send(err)
    res.json(user)
  })
}

exports.create_a_user = function (req, res) {
  var new_user = new User(req.body)
  new_user.save(function (err, user) {
    if (err) res.send(err)
    res.json(user)
  })
}

exports.read_a_user = function (req, res) {
  User.findById(req.params.userId, function (err, user) {
    if (err) res.send(err)
    res.json(user)
  }) 
}

exports.update_a_user = function (req, res) {
 User.findByIdAndUpdate(req.params.userId, req.body, function (err, user) {
    if (err) return next(err);
    res.json(user);
  }) 
} 
 

exports.delete_a_user = function (req, res) {
  User.remove(
    {
      _id: req.params.userId 
    },
    function (err, user) {
      if (err) res.send(err)
      res.json({ message: 'User successfully deleted' })
    }
  )
}
