const Files = require('./../models/files');
var mongoose = require('mongoose'), file = mongoose.model('Files');
 
exports.list_all_files = function (req, res) {
    Files.find({}, function (err, file) {
    if (err) res.send(err)
    res.json(file)
  })
}

exports.create_a_file = function (req, res) {
  var new_file = new Files (req.body)
  new_file.save(function (err, file) {
    if (err) res.send(err)
    res.json(file)
  })
}

exports.read_a_file = function (req, res) {
    Files.findById(req.params._id, function (err, update_a_file) {
    if (err) res.send(err)
    res.json(file)
  }) 
}

exports.update_a_file= function (req, res) {
    Files.findByIdAndUpdate(req.params._id, req.body, function (err, file) {
    if (err) return next(err);
    res.json(file);
  }) 
} 
 

exports.delete_a_file = function (req, res) {
    Files.remove(
    {
      _id: req.params._id 
    },
    function (err, file) {
      if (err) res.send(err)
      res.json({ message: 'User successfully deleted' })
    }
  )
}

