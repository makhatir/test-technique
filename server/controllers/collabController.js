const Collab = require('./../models/Collab');
var mongoose = require('mongoose'), collab = mongoose.model('Collab');
 
exports.list_all_collabs = function (req, res) {
  Collab.find({}, function (err, collab) {
    if (err) res.send(err)
    res.json(collab)
  })
}

exports.create_a_collab = function (req, res) {
  var new_collab = new Collab (req.body)
  new_collab.save(function (err, collab) {
    if (err) res.send(err)
    res.json(collab)
  })
}

exports.read_a_collab = function (req, res) {
  Collab.findById(req.params._id, function (err, collab) {
    if (err) res.send(err)
    res.json(collab)
  }) 
}

exports.update_a_collab= function (req, res) {
  Collab.findByIdAndUpdate(req.params._id, req.body, function (err, collab) {
    if (err) return next(err);
    res.json(collab);
  }) 
} 
 

exports.delete_a_collab = function (req, res) {
  Collab.remove(
    {
      _id: req.params._id 
    },
    function (err, collab) {
      if (err) res.send(err)
      res.json({ message: 'User successfully deleted' })
    }
  )
}

