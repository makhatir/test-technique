const Posts = require('./../models/posts');
var mongoose = require('mongoose'), post = mongoose.model('Posts');
 
exports.list_all_posts = function (req, res) {
    Posts.find({}, function (err, post) {
    if (err) res.send(err)
    res.json(post)
  })
}

exports.create_a_post = function (req, res) {
  var new_post = new Posts (req.body)
  new_post.save(function (err, post) {
    if (err) res.send(err)
    res.json(post)
  })
}

exports.read_a_post = function (req, res) {
    Posts.findById(req.params._id, function (err, update_a_post) {
    if (err) res.send(err)
    res.json(post)
  }) 
}

exports.update_a_post= function (req, res) {
    Posts.findByIdAndUpdate(req.params._id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  }) 
} 
 

exports.delete_a_post = function (req, res) {
    Posts.remove(
    {
      _id: req.params._id 
    },
    function (err, post) {
      if (err) res.send(err)
      res.json({ message: 'User successfully deleted' })
    }
  )
}

