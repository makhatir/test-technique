const File= require('../../models/files');
const Post= require('../../models/posts');
const mongoose = require('mongoose');
module.exports = (app) => {
	var files = require('../../controllers/fileController');
	var posts = require('../../controllers/postController');
	  
	
 app.route('/api/account/file')
   .get(files.list_all_files)
   .post(files.create_a_file);

 app
   .route('/api/account/file/:_id')
   .get(files.read_a_file)
   .put(files.update_a_file)
   .delete(files.delete_a_file);

   	
 app.route('/api/account/post')
 .get(posts.list_all_posts)
 .post(posts.create_a_post);

app
 .route('/api/account/post/:_id')
 .get(posts.read_a_post)
 .put(posts.update_a_post)
 .delete(posts.delete_a_post);

};