const Collab= require('../../models/Collab');
const UserSession = require('../../models/UserSession');
const mongoose = require('mongoose');
module.exports = (app) => {
   
    // -----------------api post signin---------------------------
    app.post('/api/account/signin', (req, res, next) => {
        const { body } = req;
        let {
            email
        } = body;

        if(!email) {
            return res.send({
                success: false,
                message: 'Error: Email cannot be blank.'
            }); 
         }
      
         email = email.toLowerCase();

         Collab.find({
            email: email
        }, (err, users) => {
            if(err) {
                return res.send({
                    success: false,
                    message: 'Error: server error'
                });
            }
            if(users.length != 1) {
                return res.send({
                    success: false,
                    message: 'Error: Invalid'
                });
                
            }
            const user = users[0];
        
            
            //Otherwise correct user
            const userSession = new UserSession();
            userSession.userId = user._id;
            userSession.username = user.nom;
            userSession.email = user.email;
            userSession.type = user.type;
            userSession.testP = user.testP;
                        
            userSession.save((err, doc) => {
                
                if(err) {
                    return res.send({
                        success: false,
                        message: 'Error: server error'
                    });
                }
                
                return res.send({
                    success: true,
                    message: 'Valid sign in',
                    token: doc._id,
             
                    
                });
                
            });
        });
    });
    // -----------------api post verify---------------------------
    app.get('/api/account/verify', (req, res, next) => {
        //Get the token
        const { query } = req;
        const { token } = query;
        

        UserSession.find({
            _id: token,
      
            isDeleted: false
        }, (err, sessions) => {
            if (err){
                return res.send({
                    success: false,
                    message: 'Error: Server error'
                });
            }
            if(sessions.length != 1) {
                return res.send({
                    success: false,
                    message: 'Error: Invalid'
                });
            } else {
                return res.send({
                    success: true,
                    message: 'Good'
                });
            }
        });
    });
    // -----------------api get signin---------------------------
        app.get('/api/account/signin', (req, res, next) => {
        //Get the token
        const { query } = req;
        const { token } = query;
        
        
        UserSession.find({
            _id: token,
            
            
           
            isDeleted: false
        }, (err, sessions) => {
            if (err){
                return res.send({
                    success: false,
                    message: 'Error: Server error'
                });
            }
            if(sessions.length != 1) {
                return res.send({
                    success: false,
                    message: 'Error: Invalid'
                });
            } else {
                return res.send({
                    success: true,
                    message: 'Good'
                });
            }
        });
    });
// -----------------api get logout---------------------------
    app.get('/api/account/logout', (req, res, next) => {
        //Get the token
        const { query } = req;
        const { token } = query;
        // ?token=test

        //Verify the token is one of a kind and it's not deleted.

        UserSession.findOneAndUpdate({
            _id: token,
            isDeleted: false
        }, {
            $set: {
                isDeleted:true
                }
        }, null, (err, sessions) => {
            if (err) {
                return res.send({
                    success: false,
                    message: 'Error: Server error'
                });
            }

            return res.send({
                success: true,
                message:'Good'
            });       
        });
    });
    
 
    

 
    
    
 
 
    

};
