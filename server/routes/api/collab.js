const Collab= require('../../models/Collab');
const mongoose = require('mongoose');
module.exports = (app) => {
    var collabs = require('../../controllers/collabController');
    // Categories routes
 app.route('/api/account/collab')
   .get(collabs.list_all_collabs)
   .post(collabs.create_a_collab);

 app
   .route('/api/account/collabs/:_id')
   .get(collabs.read_a_collab)
   .put(collabs.update_a_collab)
   .delete(collabs.delete_a_collab);


};