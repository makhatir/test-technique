 
const Qcm = require('../../models/Qcm');
const mongoose = require('mongoose');
module.exports = (app) => {
  var qcms = require('../../controllers/qcmsController');
     // Categories routes
  app.route('/api/account/qcm')
    .get(qcms.list_all_qcms)
    .post(qcms.create_a_qcm);

  app
    .route('/api/account/qcms/:_id')
    .get(qcms.read_a_qcm)
    .put(qcms.update_a_qcm)
    .delete(qcms.delete_a_qcm);
    

    //api QCM

    app.post('/api/account/qcms', (req, res, next) => {
 
        const { body } = req;
       
        const {
            auteur,
            categorie,
            description,
            duree,
            score,
            src,
            level
        } = body;
        let {
            titre
        } = body; 
        console.log('body', body);
        if(!titre) {
            return res.send({
                success: false,
                message: 'Error: titre cannot be blank.'
            }); 
        }
        
        if(!description) {
            return res.send({
                success: false,
                message: 'Error: description cannot be blank.'
            }); 
         }

         console.log('here');

        titre = titre.toLowerCase();
     
            
         // Steps:
         // 1. Verify titre QCM dosen't exist
         // 2. Save
         Qcm.find({
             titre: titre
         }, (err, previousQcm) => {
            if(err) {
                return res.send({
                success: false,
                message: 'Error: Server error'
            }); 
            } else if (previousQcm.length > 0) {
                return res.send({
                success: false,
                message: 'Error: Qcm already exist.'
            }); 
            }

            // Save the new categorie
            const newQcm = new Qcm();

  
 
       
            newQcm.auteur = auteur;
            newQcm.categorie = categorie;
            newQcm.description = description;
            newQcm.titre = titre;
            newQcm.duree = duree;
            newQcm.score = score;
            newQcm.src = src;
            newQcm.level = level;
            newQcm.save((err, qcm) => {
                if (err) {
                    return res.send({
                        success: false,
                        message: 'Error: Server error'
                    }); 
                }
                return res.send({
                    success: true,
                    message: 'qcm added'
                }); 
            });
                
         
         });
    });
    
 //api Question

    app.post('/api/account/questions', (req, res, next) => {

        const { body } = req;
       
        const {
             
            id_question
        } = body;
        let {
            question
        } = body;
        console.log('body', body);
        if(!question) {
            return res.send({
                success: false,
                message: 'Error: question cannot be blank.'
            }); 
        }
            /* if(!duree) {
            return res.send({
                success: false,
                message: 'Error: duree cannot be blank.'
            }); 
        }
             if(!level) {
            return res.send({
                success: false,
                message: 'Error: level cannot be blank.'
            }); 
        }*/
        if(!question) {
            return res.send({
                success: false,
                message: 'Error: question cannot be blank.'
            }); 
         }

         console.log('here');

        question = question.toLowerCase();
            
             
         // Steps:
         // 1. Verify titre Categorie dosen't Question
         // 2. Save
         Question.find({
            
             
             question: question
         }, (err, previousQuestion) => {
            if(err) {
                return res.send({
                success: false,
                message: 'Error: Server error'
            }); 
            } else if (previousQuestion.length > 0) {
                return res.send({
                success: false,
                message: 'Error: Question already exist.'
            }); 
            }
            
             
          
            // Save the new question
            const newQuestion = new Question();

            newQuestion.id_question = Question.id_question;
            newQuestion.question = question;
             
            newQuestion.save((err, question) => {
                if (err) {
                    return res.send({
                        success: false,
                        message: 'Error: Server error'
                    }); 
                }
                return res.send({
                    success: true,
                    message: 'question added'
                }); 
            });
         });
    });  
 

    
 
 
    

};
