const mongoose = require('mongoose');

 
const CollabSchema= new mongoose.Schema({
  nom: {
    type: String,
    default: ''
  },
  prenom: {
      type: String,
      default:''
  }, 
  email: {
    type: String,
    default:''
},
  
  agence: {
    type: String,
    default: ''
  },
  testP: {
    type: String,
    default: ''
  },
  type: {
    type: [{
      type: String,
      enum: ['user', 'admin']
    }],
    default: ['user']
  },
  isDeleted: {
    type: Boolean,
    default: false
  }
});
module.exports = mongoose.model('Collab', CollabSchema);
