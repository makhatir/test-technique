const mongoose = require('mongoose');

 
const PostSchema= new mongoose.Schema({
  from: {
    type: String,
    default: ''
  },
  to: {
      type: String,
      default:''
  }, 
  message: {
    type: String,
    default:''
},
  
  file: [], 
});
module.exports = mongoose.model('Posts', PostSchema);
