const mongoose = require('mongoose');

var ChoixMultipleSchema = new mongoose.Schema({
rep: {
    type: String,
    default:''
}

})

var QuestionSchema = new mongoose.Schema({
    question: {
    type: String,
    default: ''
  },
  Rep :[ChoixMultipleSchema],

})

const QcmSchema = new mongoose.Schema({

  auteur: {
    type: String,
    default: ''
  },
  titre: {
    type: String,
    default: ''
  },
  description: {
      type: String,
      default:''
  }, 
      duree: {
      type: Number,
      default:''
  },
  score: {
    type: Number,
    default:''
},
src: {
  type: String,
  default:''
},
        level: {
      type: String,
      default:''
  },
    
         categorie: {
      type: String,
      default:''
  },
    questions: [{
      id:Number,
    question: String,
    correct:String,
    reponse:[{id:String ,rep: String}]
  }],
  
  isDeleted: {
    type: Boolean,
    default: false
  }
});
module.exports = mongoose.model('Qcms', QcmSchema);
