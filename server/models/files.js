const mongoose = require('mongoose');

 
const FileSchema= new mongoose.Schema({
  name: {
    type: String,
    default: ''
  },
  originalName: {
      type: String,
      default:''
  }, 
  mimeType: {
    type: String,
    default:''
},
  
  size: {
    type: String,
    default: ''
  },
  created: {
    type: Date,
    default: Date.now()
  }
});
module.exports = mongoose.model('Files', FileSchema);
