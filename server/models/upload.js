const mongoose = require('mongoose');

 
const UploadSchema = new mongoose.Schema({
  filename: {
    type: String,
    default: ''
  },
  isDeleted: {
    type: Boolean,
    default: false
  }
});
module.exports = mongoose.model('Uploads', UploadSchema);
