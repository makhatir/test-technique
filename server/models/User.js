var mongoose = require('mongoose');
var bcrypt = require('bcrypt');

var UserSchema = new mongoose.Schema({
    
    userId: {
    type: String,
    default: ''
  },
  nom: {
    type: String,
    default: ''
  },
  prenom: {
    type: String,
    default: ''
  },
  email: {
      type: String,
      default:''
  },
  password: {
    type: String,
    default: ''
  },
  qcmP: [{Qcm: String,
      Score:String,
        dateP: {
    type: Date,
    default: Date.now()
  },
                }],
    type: {
    type: [{
      type: String,
      enum: ['user', 'admin']
    }],
    default: ['user']
  },
    
  isDeleted: {
    type: Boolean,
    default: false
  }
});

UserSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
}

UserSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
}
module.exports = mongoose.model('Users', UserSchema);