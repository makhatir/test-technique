import React from 'react';
import { connect } from 'react-redux';
import NewsDetails from './NewsDetails';

import * as newssActions from '../../actions/newsActions';
 

class NewsDetailsPage extends React.Component{
  constructor(props, context){
    super(props, context);
  
  }

  componentDidMount(){
    this.props.mappedfetchNewsById(this.props.params.id);
  }

 

  render(){
    return(
      <div>
     <h1>Article Details  </h1>
     <NewsDetails newss={this.props.mappednews} />
      </div>
    );
  };
}

const mapStateToProps = (state,ownProps) => {
  return {
     mappednews: state.newss,
   
  }
}
 const mapDispatchToProps = (dispatch) => {
   return {
     
     // the Ajax request we setup
     // in our actions
     mappedfetchNewsById: newsId => dispatch(newssActions.fetchNewsById(newsId-1)),
      
   }
 }

 export default connect(mapStateToProps, mapDispatchToProps)(NewsDetailsPage);
