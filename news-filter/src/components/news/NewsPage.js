import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router'
//import PropTypes from 'prop-types'
import { Alert,Glyphicon,Button,Modal,Table } from 'react-bootstrap';
import {bindActionCreators} from 'redux';

import * as newsActions from '../../actions/newsActions';
 
import * as appActions from '../../actions/appActions';
 
 
import './NewsPage.css';
import $ from 'jquery';
import DataTable from 'datatables.net';
$.DataTable = DataTable;
class NewsPage extends React.Component {
   constructor(props){
     super(props);
     
 
   }
  componentWillMount() {
    this.props.fetchNews();
    this.props.mappedAppSate.showAddNew = false;
  }
  componentDidUpdate(){
  
  }
 
 

 
  render(){
       $(document).ready( function () {
      setTimeout(function(){
        $('#ListNews').DataTable();
      }, 400);
    });

    const { isFetching, news} = this.props.newsList;
 
 
    const MapAppState  = this.props.mappedAppSate;
 
    const isEmpty = news.length === 0;
      

    
 
    
    if (isEmpty && isFetching ) {
      return <h2><i>Loading...</i></h2>
    }
   
 
    return(
      <div className="BookPageDiv">
 
        <h1>NEWS</h1>
<Table id="ListNews" className="table newsTable"  >
            <thead className="thead-light">
              <tr>
                <th>Article</th>
                <th>Published Date </th>
 
              </tr>
              </thead>
            <tbody>      
            {news.map(b =>
              <tr key={b.id}>
                <td  scope="row">
 
          <Link to={`/news/${b.id}`}>
          <img src={b.image.url}/> 
          </Link>
 

                </td> 
 
                 <td>  <strong>{b.titleNoFormatting}</strong> <br/> {b.content} <br/> 
                <strong>Publisher:</strong> {b.publisher}<br/>
                  <strong>Published Date:</strong>{b.publishedDate}<br/> 
                  <Link to={b.unescapedUrl}>  
                  <Button  bsStyle="danger" bsSize="xsmall">More info  </Button>
                 
                 </Link>
           </td>
              </tr>
            )}
            </tbody>
          </Table>     
     
       
      </div>
    );
  }
}

// Map state from store to props
const mapStateToProps = (state,ownProps) => {
  return {
    // You can now say this.props.news
    newsList: state.news.newsList,
  
    mappedAppSate: state.appState,
  }
};

// Map actions to props
const mapDispatchToProps = (dispatch) => {
  return {
    // You can now say this.props.createBook
 
    fetchNews: () => dispatch(newsActions.fetchNews()),
 
  }
}
 
export default connect(mapStateToProps, mapDispatchToProps)(NewsPage);
