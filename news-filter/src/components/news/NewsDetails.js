// ./src/components/book/BookDetails.js
import React from 'react';
import ReactPDF from 'react-pdf';
import './NewsPage.css'
 import { Link } from 'react-router'
 
import { Alert,Glyphicon,Button,Modal } from 'react-bootstrap';

class NewsDetails extends React.Component {
    constructor(props) {
      super(props);

      
        this.state = {
       
 
       newsa:[],
       related:[]
       
    }; 
 

   }

 
  

  componentDidUpdate(){
      this.setState({ newsa:this.props.newss['image'] });
       this.setState({ related:this.props.newss['relatedStories'] });
 

  }
render(){
 
  const  b  = this.props.newss;
   

  return (
    <div className="bookDetail">
        <div className="col-md-7">
 
                   <img src={this.state.newsa.url}/> 
 
        </div>
        <div className="col-md-5">
          <h4 className="">{b.titleNoFormatting}</h4>
          <ul className="list-group">
            <li><strong>Details </strong> {b.content}</li>
            <li><strong>publisher: </strong> {b.publisher}</li>
            <li><strong>publishedDate: </strong> {b.publishedDate} </li>
            <br/>
            
          
          </ul>
        </div>
          <div className="col-md-12">

          <h2>Related Stories </h2>
            {
              this.state.related.map( k =>
                   <a href={k.unescapedUrl}>
           
          
                  <div className="row linked">
                  <div className="col-md-6">{k.titleNoFormatting}  </div>
                  <div className="col-md-2">{k.publisher}  </div>
                  <div className="col-md-4">{k.publishedDate}  
                   
                  </div>
                   
                  </div>
                  </a>
                )
            }
          </div>
      </div>
  )
}

}

export default NewsDetails;
