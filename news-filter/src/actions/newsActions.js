 
import Axios from 'axios';

//API URL
const apiUrl = 'http://localhost:3000/';

 

export const fetchNewsRequest = () => {
  return {
    type:'FETCH_NEW_REQUEST'
  }
}


//Sync action
export const fetchNewsSuccess = (news) => {
  return {
    type: 'FETCH_NEW_SUCCESS',
    news: news,
    receivedAt: Date.now
  }
};

//Async action
export const fetchNews = () => {
  // Returns a dispatcher function
  // that dispatches an action at later time
  return (dispatch) => {

    dispatch(fetchNewsRequest());
    // Returns a promise
    return Axios.get(apiUrl + 'mocks/mocks.json')
                .then(response => {
                  // dispatch another action
                  // to consume data
                  dispatch(fetchNewsSuccess(response.data.results))
                })
                .then(error => {
                  throw(error);
                })
  }
}


 

 



//Sync action
export const fetchNewsByIdSuccess = (newss) => {
  return {
    type: 'FETCH_NEW_BY_ID_SUCCESS',
    newss
  }
}

//Async action
export const fetchNewsById = (newsId) => {
  //Return action
  return (dispatch) => {
    return Axios.get(apiUrl + 'mocks/mocks.json')
                .then(response => {
                  //Handle data with sync action
                  dispatch(fetchNewsByIdSuccess(response.data.results[newsId]))
                })
                .catch(error => {
                  throw(error);
                })
  }
};

 