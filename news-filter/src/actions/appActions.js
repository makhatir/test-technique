// ./src/actions/appActions.js

const apiUrl = "/api/";

export const toggleAddNew = () => {
  return {
    type: 'TOGGLE_ADD_NEW'
  }
}
