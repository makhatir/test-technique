// ./src/routes.js
import React from 'react';
import { Route, IndexRoute } from 'react-router';
import Home from './components/common/HomePage';
import NewsPage from './components/news/NewsPage';
import NewsDetailsPage from './components/news/NewsDetailsPage'
import App from './App';
 

export default (
  <Route path="/" component={App}>
    <IndexRoute component={NewsPage}></IndexRoute>
    <Route path="/news" component={NewsPage}></Route>
    <Route path="/news/:id" component={NewsDetailsPage}></Route>
 
  </Route>
);
