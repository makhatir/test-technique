// ./src/reducers/bookReducers.js
 
const INITIAL_STATE = { newsList: {news: [], error:null, isFetching: false},
							newNews:{newss:null, error: null, isAdding: false},
						 
							 
						};

// For handling array of books
export const newsReducer =  (currentState = INITIAL_STATE, action) => {
  switch (action.type) {
 
  case 'FETCH_NEW_REQUEST':
         return { ...currentState, newsList: {news:[], error: null, isFetching: true} };

   case 'FETCH_NEW_SUCCESS':
          //return action.books;
           return { ...currentState, newsList: {news: action.news, error:null, isFetching: false} };
  

    default:
        return currentState;

  }
};

 

export const newssReducer = (currentState = [], action) => {
  switch (action.type) {
    case 'FETCH_NEW_BY_ID_SUCCESS':
      return action.newss;
    default:
      return currentState;
  }
};
