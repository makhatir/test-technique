// ./src/reducers/index.js
import { combineReducers } from 'redux';
import { newsReducer, newssReducer } from './newsReducers';
 
import appReducer from './appReducers';
 

export default combineReducers({
  news: newsReducer,
  newss: newssReducer,
 
  
  appState: appReducer,

  // More reducers if there are
  // can go here
});
