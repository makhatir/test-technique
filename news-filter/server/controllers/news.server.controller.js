import mongoose from 'mongoose';
import multer from 'multer';
import fs from 'fs';
//import models
import  News from '../models/news.server.model';
 

 
 export const getNews = (req,res,next) => {
         News.find().exec((err,news) => {
           if(err){
           return res.json({'message':'Some Error'});
           }

           return res.json({'message':'News fetched successfully',news});
         })
}

export const getNewsById = (req,res) => {
  News.find({_id:req.params.id}).exec((err,news) => {
    if(err){
    return res.json({'success':false,'message':'Some Error'});
    }
    if(news.length){
      return res.json({'success':true,'message':'News fetched by id successfully',news});
    }
    else{
      return res.json({'success':false,'message':'News with the given id not found'});
    }
  })
}

 

 