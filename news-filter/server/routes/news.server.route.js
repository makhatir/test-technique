import express from 'express';
import * as  newsController from '../controllers/news.server.controller';


const router = express.Router();
 

router.route('/')
      .get(newsController.getNews)
      .post(newsController.addNews)
      .put(newsController.editNews);


router.route('/:id')
      .get(newsController.getNewsById)
      .delete(newsController.deleteNews);



export default router;
