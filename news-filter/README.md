# News ReactJS / Redux / NodeJS filter

The purpose of this exercise is to develop a news filter:

1. To propose a project structure, choose your dependencies and feel free to organize your folders any way that is logical and convenient for you.
2. To separate your UI into ReactJS components.
3. To use Redux for state management.
4. To Build Node.js RESTful APIs.
5. To consume the API with ReactJS components.
4. To develop a user interface for filtering a list of news.

## General guidelines

* Try do as much as you can, you can still do well even if everything is not finished.
* You will be evaluated based not only on whether the exercises are finished and work, but on code style as well. Beware of separation of concerns, abstraction, clean code, relevant comments, etc.
* **JavaScript (ES6 allowed), HTML (HTML5 allowed) and CSS (CSS 3 allowed).**
* Use the provided mock list of news (mocks/mocks.json).

To perform the technical test the developer needs:

* NodeJs and Npm installed and well configured
* An IDE to work (Eclipse, IntelliJ, Atom..)
* Basic knowledge of NodeJS, ReactJS and NPM to set up the exercise. 

## Your tasks

** Read the readme carefully and check the signatures imposed by the tests. **

* Install the necessary packages using npm
* Fetch the data and render it on the page. 
* Initially the page should show only a list of titles fetched from the source. 
* On clicking the title it should show the relevant image and content of the article associated with the title as well as related stories, if available. 
* There should be a “read more” link to the article source. 
* Above the list of articles there should be a search field for filtering the articles. 
* Upon typing either the title or a keyword from the article content the list should be filtered to show only relevant results from an already rendered content. 
* The filtering should occur as the user is typing. Additionally the user should be able to filter by date published.

### NodeJS

For this exercise  you have to build an API that performs full READ on a single resource - news.

The API will consist of the following 3 GET routes :

	* GET /news - returns an array of news objects (have to be used to fetch the news and show them on the page)
	* GET /news/:id - returns the relevant object base on id parametere (have to be used to show the concerned news details page)
	* GET /news/:searchCriteria - returns a list of objetcs that match the search criteria (have to be used to filter the news list)


**You could include Express js with your project dependencies, it will save your time ;) **

### ReactJS

Thinking in React, it's up to you to decide which Components to code / structure to implement. Don't forget to consume the API ;)

### Redux

Use Redux for state management, reducers actions store... Here we go! :D

### CSS - UI

* Showcase your css skills by making it looking nice.


## Restrictions:

* The recommended duration of the test is 4 hours
* Code should be clean, modular and reusable.

## Bonus points for and including, but not limited to:

* Implementing Server Side Rendering, Yes why not ?
* Showcasing your CSS skills by not using Bootstrap or similar libraries.
* Making it app mobile friendly
* Making the application flexible, e.g. implementing options for number of items shown, options for showing or hiding related news etc.
* Making code testable

## Copyright

This exercise is the property of SQLI Morocco.

You are not authorized to use the exercise or any part thereof in any context other than the current recruitment session.

You are not authorized to publish the exercise or any part thereof on any platform, whether public or internal.

## Good luck!